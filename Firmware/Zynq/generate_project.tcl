# SETUP project
set prj_name "PiLUP_zynq"
set root_dir [file dirname [info script]]
set prj_dir "${root_dir}/${prj_name}"
set top_name "PiLUP"
create_project $prj_name $prj_dir -part xc7z020clg484-1  
set_property board_part xilinx.com:zc702:part0:1.3 [current_project] 
set_property target_language VHDL [current_project]
add_files -fileset constrs_1 -norecurse "${root_dir}/xdc/constr.xdc"

##################################################################################
############################# Create block design ################################
##################################################################################
create_bd_design $top_name

#################################  IP cells  ##################################

create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7 processing_system7_0
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config \
	{make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {200} CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_IRQ_F2P_INTR {1}] \
	[get_bd_cells processing_system7_0]

create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset proc_sys_reset_0 

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_chip2chip axi_chip2chip_0
set_property -dict [list CONFIG.C_USE_DIFF_IO {true} CONFIG.C_USE_DIFF_CLK {true}] [get_bd_cells axi_chip2chip_0]


######################## IP cells interconnections ############################

# connect axi interfaces to c2c with smartconnect
create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect smartconnect_0
set_property -dict [list CONFIG.NUM_MI {1} CONFIG.NUM_SI {1}] [get_bd_cells smartconnect_0]
connect_bd_intf_net [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins smartconnect_0/S00_AXI]
connect_bd_intf_net [get_bd_intf_pins smartconnect_0/M00_AXI] [get_bd_intf_pins axi_chip2chip_0/s_axi]

# clock and reset "tree"
connect_bd_net [get_bd_pins [list \
	/processing_system7_0/FCLK_CLK0 \
	/proc_sys_reset_0/slowest_sync_clk \
	/axi_chip2chip_0/idelay_ref_clk \
	/axi_chip2chip_0/axi_c2c_phy_clk \
	[get_bd_pins */*aclk]]]
connect_bd_net [get_bd_pins [ list \
	/proc_sys_reset_0/peripheral_aresetn \
	[get_bd_pins  */*aresetn -filter {DIR == I}]]]
connect_bd_net [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins proc_sys_reset_0/ext_reset_in]

# intrs (with PS IRQ workaround)
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat concat_IRQ_wrkaround
set_property -dict [list CONFIG.NUM_PORTS {1} CONFIG.IN0_WIDTH {4}] [get_bd_cells concat_IRQ_wrkaround]
connect_bd_net [get_bd_pins concat_IRQ_wrkaround/dout] [get_bd_pins processing_system7_0/IRQ_F2P]
connect_bd_net [get_bd_pins concat_IRQ_wrkaround/In0] [get_bd_pins axi_chip2chip_0/axi_c2c_s2m_intr_out]


############################ External ports ###################################

# C2C
create_bd_port -dir I -type clk KZ_CLK_IN_P 
set_property CONFIG.FREQ_HZ 200000000 [get_bd_ports KZ_CLK_IN_P]
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_clk_in_p] [get_bd_ports KZ_CLK_IN_P] 
create_bd_port -dir I -type clk KZ_CLK_IN_N 
set_property CONFIG.FREQ_HZ 200000000 [get_bd_ports KZ_CLK_IN_N]
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_clk_in_n] [get_bd_ports KZ_CLK_IN_N] 
create_bd_port -dir O -type clk KZ_BUS_CLK_P 
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_clk_out_p] [get_bd_ports KZ_BUS_CLK_P]   
create_bd_port -dir O -type clk KZ_BUS_CLK_N 
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_clk_out_n] [get_bd_ports KZ_BUS_CLK_N]   
create_bd_port -dir I -from 8 -to 0 AXI_C2C_IN_P
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_data_in_p] [get_bd_ports AXI_C2C_IN_P]
create_bd_port -dir I -from 8 -to 0 AXI_C2C_IN_N
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_data_in_n] [get_bd_ports AXI_C2C_IN_N]
create_bd_port -dir O -from 8 -to 0 AXI_C2C_OUT_P
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_data_out_p] [get_bd_ports AXI_C2C_OUT_P]
create_bd_port -dir O -from 8 -to 0 AXI_C2C_OUT_N
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_data_out_n] [get_bd_ports AXI_C2C_OUT_N]
# reset
create_bd_port -dir O -type rst slave_rst
connect_bd_net [get_bd_ports slave_rst] [get_bd_pins processing_system7_0/FCLK_RESET0_N]

##################### AXI ADDRESS assigments ################################
assign_bd_address [get_bd_addr_segs {axi_chip2chip_0/s_axi/Mem0 }]
set_property range 256K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_chip2chip_0_Mem0}]

# fix ugliness and save bd
regenerate_bd_layout
save_bd_design
# generate hdl wrapper
make_wrapper -files [get_files ${prj_dir}/${prj_name}.srcs/sources_1/bd/${top_name}/${top_name}.bd] -top
add_files -norecurse ${prj_dir}/${prj_name}.srcs/sources_1/bd/$top_name/hdl/${top_name}_wrapper.vhd
