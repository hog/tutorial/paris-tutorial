#clocks
set_property IOSTANDARD LVDS [get_ports SYSCLK_N]
set_property PACKAGE_PIN AD12 [get_ports SYSCLK_P]
set_property PACKAGE_PIN AD11 [get_ports SYSCLK_N]
set_property IOSTANDARD LVDS [get_ports SYSCLK_P]


# AXI C2C master to slave CLK
set_property IOSTANDARD LVDS_25 [get_ports KZ_BUS_CLK_*]
set_property DIFF_TERM true [get_ports KZ_BUS_CLK_*]
set_property PACKAGE_PIN AG24 [get_ports KZ_BUS_CLK_P]
set_property PACKAGE_PIN AH24 [get_ports KZ_BUS_CLK_N]


# AXI C2C slave to master bus
set_property IOSTANDARD LVDS_25 [get_ports AXI_C2C_OUT_*]
set_property DIFF_TERM true [get_ports AXI_C2C_OUT_*]
#set_property PACKAGE_PIN Y21  [get_ports AXI_C2C_OUT_P[0]]
#set_property PACKAGE_PIN AA21 [get_ports AXI_C2C_OUT_N[0]]
set_property PACKAGE_PIN AB22 [get_ports AXI_C2C_OUT_P[0]]
set_property PACKAGE_PIN AB23 [get_ports AXI_C2C_OUT_N[0]]
set_property PACKAGE_PIN AA22 [get_ports AXI_C2C_OUT_P[1]]
set_property PACKAGE_PIN AA23 [get_ports AXI_C2C_OUT_N[1]]
set_property PACKAGE_PIN AC20 [get_ports AXI_C2C_OUT_P[2]]
set_property PACKAGE_PIN AC21 [get_ports AXI_C2C_OUT_N[2]]
set_property PACKAGE_PIN AA25 [get_ports AXI_C2C_OUT_P[3]]
set_property PACKAGE_PIN AB25 [get_ports AXI_C2C_OUT_N[3]]
set_property PACKAGE_PIN AA27 [get_ports AXI_C2C_OUT_P[4]]
set_property PACKAGE_PIN AB28 [get_ports AXI_C2C_OUT_N[4]]
set_property PACKAGE_PIN W29  [get_ports AXI_C2C_OUT_P[5]]
set_property PACKAGE_PIN Y29  [get_ports AXI_C2C_OUT_N[5]]
set_property PACKAGE_PIN Y28  [get_ports AXI_C2C_OUT_P[6]]
set_property PACKAGE_PIN AA28 [get_ports AXI_C2C_OUT_N[6]]
set_property PACKAGE_PIN Y26  [get_ports AXI_C2C_OUT_P[7]]
set_property PACKAGE_PIN AA26 [get_ports AXI_C2C_OUT_N[7]]
set_property PACKAGE_PIN L17  [get_ports AXI_C2C_OUT_P[8]]
set_property PACKAGE_PIN L18  [get_ports AXI_C2C_OUT_N[8]]


# AXI C2C slave to master CLK
set_property IOSTANDARD LVDS_25 [get_ports KZ_CLK_OUT_*]
set_property PACKAGE_PIN J19 [get_ports KZ_CLK_OUT_P]
set_property PACKAGE_PIN H19 [get_ports KZ_CLK_OUT_N]


# AXI C2C master to slave bus
set_property IOSTANDARD LVDS_25 [get_ports AXI_C2C_IN_*]
#set_property PACKAGE_PIN Y23 [get_ports {AXI_C2C_OUT_P[0]}]
#set_property PACKAGE_PIN Y24 [get_ports {AXI_C2C_OUT_N[0]}]
set_property PACKAGE_PIN J17 [get_ports AXI_C2C_IN_P[0]]
set_property PACKAGE_PIN H17 [get_ports AXI_C2C_IN_N[0]]
set_property PACKAGE_PIN H20 [get_ports AXI_C2C_IN_P[1]]
set_property PACKAGE_PIN G20 [get_ports AXI_C2C_IN_N[1]]
set_property PACKAGE_PIN K18 [get_ports AXI_C2C_IN_P[2]]
set_property PACKAGE_PIN J18 [get_ports AXI_C2C_IN_N[2]]
set_property PACKAGE_PIN G23 [get_ports AXI_C2C_IN_P[3]]
set_property PACKAGE_PIN G24 [get_ports AXI_C2C_IN_N[3]]
set_property PACKAGE_PIN F26 [get_ports AXI_C2C_IN_P[4]]
set_property PACKAGE_PIN E26 [get_ports AXI_C2C_IN_N[4]]
set_property PACKAGE_PIN E24 [get_ports AXI_C2C_IN_P[5]]
set_property PACKAGE_PIN D24 [get_ports AXI_C2C_IN_N[5]]
set_property PACKAGE_PIN F25 [get_ports AXI_C2C_IN_P[6]]
set_property PACKAGE_PIN E25 [get_ports AXI_C2C_IN_N[6]]
set_property PACKAGE_PIN E23 [get_ports AXI_C2C_IN_P[7]]
set_property PACKAGE_PIN D23 [get_ports AXI_C2C_IN_N[7]]
set_property PACKAGE_PIN B23 [get_ports AXI_C2C_IN_P[8]]
set_property PACKAGE_PIN A23 [get_ports AXI_C2C_IN_N[8]]


# master to slave reset
set_property IOSTANDARD LVCMOS25 [get_ports ext_reset_in]
set_property PACKAGE_PIN Y25 [get_ports ext_reset_in]
