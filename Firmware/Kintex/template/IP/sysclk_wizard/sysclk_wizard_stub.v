// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Wed Nov 17 13:34:45 2021
// Host        : atlas126a running 64-bit openSUSE Tumbleweed
// Command     : write_verilog -force -mode synth_stub
//               /home/iwsatlas1/dcieri/Work/Hog/Tutorial/PiLUP_devel/Firmware/Kintex/template/IP/sysclk_wizard/sysclk_wizard_stub.v
// Design      : sysclk_wizard
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module sysclk_wizard(clk200_out, reset, locked, clk_in1_p, clk_in1_n)
/* synthesis syn_black_box black_box_pad_pin="clk200_out,reset,locked,clk_in1_p,clk_in1_n" */;
  output clk200_out;
  input reset;
  output locked;
  input clk_in1_p;
  input clk_in1_n;
endmodule
