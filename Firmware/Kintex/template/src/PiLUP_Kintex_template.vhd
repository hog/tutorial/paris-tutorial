----------------------------------------------------------------------------------
-- Company: INFN & University of Bologna
-- Developers: Nico Giangiacomi (nico.giangiacomi@cern.ch)
--             Giuseppe Gebbia
--
-- Create Date: 06/19/2018 03:58:37 PM
-- Design Name: PiLUP_Kintex_template
-- Module Name: PiLUP_Kintex_template - Behavioral
-- Project Name: PiLUP_Kintex_template
-- Target Devices: Bologna INFN PiLUP Board
-- Tool Versions: 1.0
-- Description: template firmware - starting point for creating a new project
--              block design and register block are defined in this template
--              user logic and signals must be added in different projects
--
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.register_pkg.all;
use work.datetimehash_pkg.all;

entity PiLUP_Kintex_template is
generic (
    -- Global Generic Variables
    GLOBAL_DATE : std_logic_vector(31 downto 0);
    GLOBAL_TIME : std_logic_vector(31 downto 0);
    GLOBAL_VER  : std_logic_vector(31 downto 0);
    GLOBAL_SHA  : std_logic_vector(31 downto 0);
    TOP_VER     : std_logic_vector(31 downto 0);
    TOP_SHA     : std_logic_vector(31 downto 0);
    CON_VER     : std_logic_vector(31 downto 0);
    CON_SHA     : std_logic_vector(31 downto 0);
    HOG_VER     : std_logic_vector(31 downto 0);
    HOG_SHA     : std_logic_vector(31 downto 0);

    -- Project Specific Lists (One for each .src file in your Top/myproj/list folder)
    xil_defaultlib_VER : std_logic_vector(31 downto 0);
    xil_defaultlib_SHA : std_logic_vector(31 downto 0);
    Default_VER : std_logic_vector(31 downto 0);
    Default_SHA : std_logic_vector(31 downto 0)
);
Port (

	--  CLOCK SIGNALS
	SYSCLK_P 		: in std_logic;
	SYSCLK_N 		: in std_logic;
	
	-- IP BLOB external interfaces (C2C + i2c)
	AXI_C2C_IN_N : in std_logic_vector ( 8 downto 0 );
	AXI_C2C_IN_P : in std_logic_vector ( 8 downto 0 );
	AXI_C2C_OUT_N : out std_logic_vector ( 8 downto 0 );
	AXI_C2C_OUT_P : out std_logic_vector ( 8 downto 0 );
	KZ_BUS_CLK_N : in std_logic;
	KZ_BUS_CLK_P : in std_logic;
	KZ_CLK_OUT_N : out std_logic;
	KZ_CLK_OUT_P : out std_logic;
	ext_reset_in : in std_logic;
	iic_main_scl_io : inout std_logic;
	iic_main_sda_io : inout std_logic
	
);
end PiLUP_Kintex_template;

architecture Behavioral of PiLUP_Kintex_template is
	----------------------------------------------------------
	--                                                      --
	--                  Signal Declaration                  --
	--                                                      --
	----------------------------------------------------------
	signal status : reg_matrix(0 to N_STATUS_REGS-1):= (others => (others => '0'));
	signal ctrl : reg_matrix(0 to N_CTRL_REGS-1);

	-- Axi interface signals
	signal AXI_reg_matrix_araddr   : std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
	signal AXI_reg_matrix_arprot   : std_logic_vector (2 downto 0);
	signal AXI_reg_matrix_arready  : std_logic;
	signal AXI_reg_matrix_arvalid  : std_logic;
	signal AXI_reg_matrix_awaddr   : std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
	signal AXI_reg_matrix_awprot   : std_logic_vector (2 downto 0);
	signal AXI_reg_matrix_awready  : std_logic;
	signal AXI_reg_matrix_awvalid  : std_logic;
	signal AXI_reg_matrix_bready   : std_logic;
	signal AXI_reg_matrix_bresp    : std_logic_vector (1 downto 0);
	signal AXI_reg_matrix_bvalid   : std_logic;
	signal AXI_reg_matrix_rdata    : std_logic_vector (31 downto 0);
	signal AXI_reg_matrix_rready   : std_logic;
	signal AXI_reg_matrix_rresp    : std_logic_vector (1 downto 0);
	signal AXI_reg_matrix_rvalid   : std_logic;
	signal AXI_reg_matrix_wdata    : std_logic_vector (31 downto 0);
	signal AXI_reg_matrix_wready   : std_logic;
	signal AXI_reg_matrix_wstrb    : std_logic_vector (3 downto 0);
	signal AXI_reg_matrix_wvalid   : std_logic;
	
	
	signal iic_main_scl_i 			: std_logic;
	signal iic_main_scl_o 			: std_logic;
	signal iic_main_scl_t 			: std_logic;
	signal iic_main_sda_i 			: std_logic;
	signal iic_main_sda_o 			: std_logic;
	signal iic_main_sda_t 			: std_logic;

	signal sysclk					: std_logic;
	signal reset_from_software		: std_logic;
	
	--						USER SIGNALS					--
	
	----------------------------------------------------------
	--                                                      --
	--                 Component Declaration                --
	--                                                      --
	----------------------------------------------------------
	component IP_blob is
	generic(
		C_S_AXI_ADDR_WIDTH : integer := REGS_AXI_ADDR_WIDTH
	);
	port (
		iic_main_scl_i : in std_logic;
		iic_main_scl_o : out std_logic;
		iic_main_scl_t : out std_logic;
		iic_main_sda_i : in std_logic;
		iic_main_sda_o : out std_logic;
		iic_main_sda_t : out std_logic;
		KZ_BUS_CLK_P : in std_logic;
		KZ_BUS_CLK_N : in std_logic;
		KZ_CLK_OUT_P : out std_logic;
		KZ_CLK_OUT_N : out std_logic;
		AXI_C2C_IN_P : in std_logic_vector(8 downto 0);
		AXI_C2C_IN_N : in std_logic_vector(8 downto 0);
		AXI_C2C_OUT_P : out std_logic_vector(8 downto 0);
		AXI_C2C_OUT_N : out std_logic_vector(8 downto 0);
		AXI_CLK200  :  in std_logic;
		AXI_register_matrix_araddr : out std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
		AXI_register_matrix_arprot : out std_logic_vector (2 downto 0);
		AXI_register_matrix_arready : in std_logic;
		AXI_register_matrix_arvalid : out std_logic;
		AXI_register_matrix_awaddr : out std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
		AXI_register_matrix_awprot : out std_logic_vector (2 downto 0);
		AXI_register_matrix_awready : in std_logic;
		AXI_register_matrix_awvalid : out std_logic;
		AXI_register_matrix_bready : out std_logic;
		AXI_register_matrix_bresp : in std_logic_vector (1 downto 0);
		AXI_register_matrix_bvalid : in std_logic;
		AXI_register_matrix_rdata : in std_logic_vector (31 downto 0);
		AXI_register_matrix_rready : out std_logic;
		AXI_register_matrix_rresp : in std_logic_vector (1 downto 0);
		AXI_register_matrix_rvalid : in std_logic;
		AXI_register_matrix_wdata : out std_logic_vector (31 downto 0);
		AXI_register_matrix_wready : in std_logic;
		AXI_register_matrix_wstrb : out std_logic_vector (3 downto 0);
		AXI_register_matrix_wvalid : out std_logic;
		ext_reset_in : in std_logic
	);
	end component IP_blob;

	component IOBUF is
	port (
		I : in std_logic;
		O : out std_logic;
		T : in std_logic;
		IO : inout std_logic
	);
	end component IOBUF;
	
	
	component sysclk_wizard
	port (
		-- Clock out ports
		clk200_out          : out    std_logic;
		-- Status and control signals
		reset             : in     std_logic;
		locked            : out    std_logic;
		clk_in1_p         : in     std_logic;
		clk_in1_n         : in     std_logic
	);
	end component;
	
	--					USER COMPONENTS						--
	
begin

	----------------------------------------------------------
	--                                                      --
	--                Component Implementation              --
	--                                                      --
	----------------------------------------------------------
	register_block: entity work.axi_regs_test
	generic map(
		C_S_AXI_ADDR_WIDTH => REGS_AXI_ADDR_WIDTH
	)
	port map(
		S_AXI_ACLK	 	=> sysclk,
		S_AXI_ARESETN	=> ext_reset_in,
		S_AXI_AWADDR 	=> AXI_reg_matrix_awaddr,
		S_AXI_AWPROT 	=> AXI_reg_matrix_awprot,
		S_AXI_AWVALID	=> AXI_reg_matrix_awvalid,
		S_AXI_AWREADY	=> AXI_reg_matrix_awready,
		S_AXI_WDATA	 	=> AXI_reg_matrix_wdata,
		S_AXI_WSTRB	 	=> AXI_reg_matrix_wstrb,
		S_AXI_WVALID 	=> AXI_reg_matrix_wvalid,
		S_AXI_WREADY 	=> AXI_reg_matrix_wready,
		S_AXI_BRESP	 	=> AXI_reg_matrix_bresp,
		S_AXI_BVALID 	=> AXI_reg_matrix_bvalid,
		S_AXI_BREADY 	=> AXI_reg_matrix_bready,
		S_AXI_ARADDR 	=> AXI_reg_matrix_araddr,
		S_AXI_ARPROT 	=> AXI_reg_matrix_arprot,
		S_AXI_ARVALID	=> AXI_reg_matrix_arvalid,
		S_AXI_ARREADY	=> AXI_reg_matrix_arready,
		S_AXI_RDATA	 	=> AXI_reg_matrix_rdata,
		S_AXI_RRESP	 	=> AXI_reg_matrix_rresp,
		S_AXI_RVALID 	=> AXI_reg_matrix_rvalid,
		S_AXI_RREADY 	=> AXI_reg_matrix_rready,

		STATUS => status,
		CTRL => ctrl
	);

	IP_block_i: component IP_blob
	port map (
		AXI_C2C_IN_N => AXI_C2C_IN_N,
		AXI_C2C_IN_P => AXI_C2C_IN_P,
		AXI_C2C_OUT_N => AXI_C2C_OUT_N,
		AXI_C2C_OUT_P => AXI_C2C_OUT_P,
		KZ_BUS_CLK_N => KZ_BUS_CLK_N,
		KZ_BUS_CLK_P => KZ_BUS_CLK_P,
		KZ_CLK_OUT_N => KZ_CLK_OUT_N,
		KZ_CLK_OUT_P => KZ_CLK_OUT_P,
		ext_reset_in => ext_reset_in,
		iic_main_scl_i => iic_main_scl_i,
		iic_main_scl_o => iic_main_scl_o,
		iic_main_scl_t => iic_main_scl_t,
		iic_main_sda_i => iic_main_sda_i,
		iic_main_sda_o => iic_main_sda_o,
		iic_main_sda_t => iic_main_sda_t,
		AXI_CLK200 => sysclk,

		AXI_register_matrix_araddr  => AXI_reg_matrix_araddr,
		AXI_register_matrix_arprot  => AXI_reg_matrix_arprot,
		AXI_register_matrix_arready => AXI_reg_matrix_arready,
		AXI_register_matrix_arvalid => AXI_reg_matrix_arvalid,
		AXI_register_matrix_awaddr  => AXI_reg_matrix_awaddr,
		AXI_register_matrix_awprot  => AXI_reg_matrix_awprot,
		AXI_register_matrix_awready => AXI_reg_matrix_awready,
		AXI_register_matrix_awvalid => AXI_reg_matrix_awvalid,
		AXI_register_matrix_bready  => AXI_reg_matrix_bready,
		AXI_register_matrix_bresp   => AXI_reg_matrix_bresp,
		AXI_register_matrix_bvalid  => AXI_reg_matrix_bvalid,
		AXI_register_matrix_rdata   => AXI_reg_matrix_rdata,
		AXI_register_matrix_rready  => AXI_reg_matrix_rready,
		AXI_register_matrix_rresp   => AXI_reg_matrix_rresp,
		AXI_register_matrix_rvalid  => AXI_reg_matrix_rvalid,
		AXI_register_matrix_wdata   => AXI_reg_matrix_wdata,
		AXI_register_matrix_wready  => AXI_reg_matrix_wready,
		AXI_register_matrix_wstrb   => AXI_reg_matrix_wstrb,
		AXI_register_matrix_wvalid  => AXI_reg_matrix_wvalid
	);

	iic_main_scl_iobuf: component IOBUF
	port map(
		I => iic_main_scl_o,
		IO => iic_main_scl_io,
		O => iic_main_scl_i,
		T => iic_main_scl_t
	);
	iic_main_sda_iobuf: component IOBUF
	port map(
		I => iic_main_sda_o,
		IO => iic_main_sda_io,
		O => iic_main_sda_i,
		T => iic_main_sda_t
	);
	

	sysclk_wizard_impl: sysclk_wizard
	Port Map(
		clk200_out		=> sysclk,
		reset			=> '0',	
		locked			=> open,
		clk_in1_p		=> SYSCLK_P,
		clk_in1_n		=> SYSCLK_N
	);
	
	-- register connection
	--ctrl registers
	reset_from_software							<= ctrl(reset_from_software_REG)(reset_from_software_RANGE);
	
	-- status registers
	status(DATE_HEX_REG)(DATE_HEX_RANGE)		<= DATE_HEX;
	status(TIME_HEX_REG)(TIME_HEX_RANGE)		<= TIME_HEX;
	status(GIT_HASH_0_REG)(GIT_HASH_0_RANGE)	<= GIT_HASH_0;
	status(GIT_HASH_1_REG)(GIT_HASH_1_RANGE)	<= GIT_HASH_1;
	status(GIT_HASH_2_REG)(GIT_HASH_2_RANGE)	<= GIT_HASH_2;
	status(GIT_HASH_3_REG)(GIT_HASH_3_RANGE)	<= GIT_HASH_3;
	status(GIT_HASH_4_REG)(GIT_HASH_4_RANGE)	<= GIT_HASH_4;
	--						USER LOGIC					--

	
	----------------------------------------------------------
	--                                                      --
	--                 Process Implementation               --
	--                                                      --
	----------------------------------------------------------
	

end Behavioral;
