-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Wed Nov 17 13:34:59 2021
-- Host        : atlas126a running 64-bit openSUSE Tumbleweed
-- Command     : write_vhdl -force -mode synth_stub
--               /home/iwsatlas1/dcieri/Work/Hog/Tutorial/PiLUP_devel/Firmware/Kintex/bd/template/IP_blob/ip/IP_blob_axi_chip2chip_0_0/IP_blob_axi_chip2chip_0_0_stub.vhdl
-- Design      : IP_blob_axi_chip2chip_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k325tffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IP_blob_axi_chip2chip_0_0 is
  Port ( 
    m_aclk : in STD_LOGIC;
    m_aresetn : in STD_LOGIC;
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awready : in STD_LOGIC;
    m_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_wlast : out STD_LOGIC;
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wready : in STD_LOGIC;
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bready : out STD_LOGIC;
    m_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arvalid : out STD_LOGIC;
    m_axi_arready : in STD_LOGIC;
    m_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rlast : in STD_LOGIC;
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rready : out STD_LOGIC;
    axi_c2c_s2m_intr_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_c2c_m2s_intr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    idelay_ref_clk : in STD_LOGIC;
    axi_c2c_selio_tx_diff_clk_out_p : out STD_LOGIC;
    axi_c2c_selio_tx_diff_clk_out_n : out STD_LOGIC;
    axi_c2c_selio_tx_diff_data_out_p : out STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_selio_tx_diff_data_out_n : out STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_selio_rx_diff_clk_in_p : in STD_LOGIC;
    axi_c2c_selio_rx_diff_clk_in_n : in STD_LOGIC;
    axi_c2c_selio_rx_diff_data_in_p : in STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_selio_rx_diff_data_in_n : in STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_link_status_out : out STD_LOGIC;
    axi_c2c_multi_bit_error_out : out STD_LOGIC
  );

end IP_blob_axi_chip2chip_0_0;

architecture stub of IP_blob_axi_chip2chip_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "m_aclk,m_aresetn,m_axi_awaddr[31:0],m_axi_awlen[7:0],m_axi_awsize[2:0],m_axi_awburst[1:0],m_axi_awvalid,m_axi_awready,m_axi_wdata[31:0],m_axi_wstrb[3:0],m_axi_wlast,m_axi_wvalid,m_axi_wready,m_axi_bresp[1:0],m_axi_bvalid,m_axi_bready,m_axi_araddr[31:0],m_axi_arlen[7:0],m_axi_arsize[2:0],m_axi_arburst[1:0],m_axi_arvalid,m_axi_arready,m_axi_rdata[31:0],m_axi_rresp[1:0],m_axi_rlast,m_axi_rvalid,m_axi_rready,axi_c2c_s2m_intr_in[3:0],axi_c2c_m2s_intr_out[3:0],idelay_ref_clk,axi_c2c_selio_tx_diff_clk_out_p,axi_c2c_selio_tx_diff_clk_out_n,axi_c2c_selio_tx_diff_data_out_p[8:0],axi_c2c_selio_tx_diff_data_out_n[8:0],axi_c2c_selio_rx_diff_clk_in_p,axi_c2c_selio_rx_diff_clk_in_n,axi_c2c_selio_rx_diff_data_in_p[8:0],axi_c2c_selio_rx_diff_data_in_n[8:0],axi_c2c_link_status_out,axi_c2c_multi_bit_error_out";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "axi_chip2chip_v5_0_9,Vivado 2020.2";
begin
end;
