// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Wed Nov 17 13:34:59 2021
// Host        : atlas126a running 64-bit openSUSE Tumbleweed
// Command     : write_verilog -force -mode synth_stub
//               /home/iwsatlas1/dcieri/Work/Hog/Tutorial/PiLUP_devel/Firmware/Kintex/bd/template/IP_blob/ip/IP_blob_axi_chip2chip_0_0/IP_blob_axi_chip2chip_0_0_stub.v
// Design      : IP_blob_axi_chip2chip_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "axi_chip2chip_v5_0_9,Vivado 2020.2" *)
module IP_blob_axi_chip2chip_0_0(m_aclk, m_aresetn, m_axi_awaddr, m_axi_awlen, 
  m_axi_awsize, m_axi_awburst, m_axi_awvalid, m_axi_awready, m_axi_wdata, m_axi_wstrb, 
  m_axi_wlast, m_axi_wvalid, m_axi_wready, m_axi_bresp, m_axi_bvalid, m_axi_bready, 
  m_axi_araddr, m_axi_arlen, m_axi_arsize, m_axi_arburst, m_axi_arvalid, m_axi_arready, 
  m_axi_rdata, m_axi_rresp, m_axi_rlast, m_axi_rvalid, m_axi_rready, axi_c2c_s2m_intr_in, 
  axi_c2c_m2s_intr_out, idelay_ref_clk, axi_c2c_selio_tx_diff_clk_out_p, 
  axi_c2c_selio_tx_diff_clk_out_n, axi_c2c_selio_tx_diff_data_out_p, 
  axi_c2c_selio_tx_diff_data_out_n, axi_c2c_selio_rx_diff_clk_in_p, 
  axi_c2c_selio_rx_diff_clk_in_n, axi_c2c_selio_rx_diff_data_in_p, 
  axi_c2c_selio_rx_diff_data_in_n, axi_c2c_link_status_out, 
  axi_c2c_multi_bit_error_out)
/* synthesis syn_black_box black_box_pad_pin="m_aclk,m_aresetn,m_axi_awaddr[31:0],m_axi_awlen[7:0],m_axi_awsize[2:0],m_axi_awburst[1:0],m_axi_awvalid,m_axi_awready,m_axi_wdata[31:0],m_axi_wstrb[3:0],m_axi_wlast,m_axi_wvalid,m_axi_wready,m_axi_bresp[1:0],m_axi_bvalid,m_axi_bready,m_axi_araddr[31:0],m_axi_arlen[7:0],m_axi_arsize[2:0],m_axi_arburst[1:0],m_axi_arvalid,m_axi_arready,m_axi_rdata[31:0],m_axi_rresp[1:0],m_axi_rlast,m_axi_rvalid,m_axi_rready,axi_c2c_s2m_intr_in[3:0],axi_c2c_m2s_intr_out[3:0],idelay_ref_clk,axi_c2c_selio_tx_diff_clk_out_p,axi_c2c_selio_tx_diff_clk_out_n,axi_c2c_selio_tx_diff_data_out_p[8:0],axi_c2c_selio_tx_diff_data_out_n[8:0],axi_c2c_selio_rx_diff_clk_in_p,axi_c2c_selio_rx_diff_clk_in_n,axi_c2c_selio_rx_diff_data_in_p[8:0],axi_c2c_selio_rx_diff_data_in_n[8:0],axi_c2c_link_status_out,axi_c2c_multi_bit_error_out" */;
  input m_aclk;
  input m_aresetn;
  output [31:0]m_axi_awaddr;
  output [7:0]m_axi_awlen;
  output [2:0]m_axi_awsize;
  output [1:0]m_axi_awburst;
  output m_axi_awvalid;
  input m_axi_awready;
  output [31:0]m_axi_wdata;
  output [3:0]m_axi_wstrb;
  output m_axi_wlast;
  output m_axi_wvalid;
  input m_axi_wready;
  input [1:0]m_axi_bresp;
  input m_axi_bvalid;
  output m_axi_bready;
  output [31:0]m_axi_araddr;
  output [7:0]m_axi_arlen;
  output [2:0]m_axi_arsize;
  output [1:0]m_axi_arburst;
  output m_axi_arvalid;
  input m_axi_arready;
  input [31:0]m_axi_rdata;
  input [1:0]m_axi_rresp;
  input m_axi_rlast;
  input m_axi_rvalid;
  output m_axi_rready;
  input [3:0]axi_c2c_s2m_intr_in;
  output [3:0]axi_c2c_m2s_intr_out;
  input idelay_ref_clk;
  output axi_c2c_selio_tx_diff_clk_out_p;
  output axi_c2c_selio_tx_diff_clk_out_n;
  output [8:0]axi_c2c_selio_tx_diff_data_out_p;
  output [8:0]axi_c2c_selio_tx_diff_data_out_n;
  input axi_c2c_selio_rx_diff_clk_in_p;
  input axi_c2c_selio_rx_diff_clk_in_n;
  input [8:0]axi_c2c_selio_rx_diff_data_in_p;
  input [8:0]axi_c2c_selio_rx_diff_data_in_n;
  output axi_c2c_link_status_out;
  output axi_c2c_multi_bit_error_out;
endmodule
