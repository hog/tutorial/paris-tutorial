--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Thu Nov 18 10:24:38 2021
--Host        : atlas126a running 64-bit openSUSE Tumbleweed
--Command     : generate_target IP_blob_wrapper.bd
--Design      : IP_blob_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity IP_blob_wrapper is
  port (
    AXI_C2C_IN_N : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_IN_P : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_OUT_N : out STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_OUT_P : out STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_CLK200 : in STD_LOGIC;
    AXI_register_matrix_araddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_register_matrix_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_register_matrix_arready : in STD_LOGIC;
    AXI_register_matrix_arvalid : out STD_LOGIC;
    AXI_register_matrix_awaddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_register_matrix_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_register_matrix_awready : in STD_LOGIC;
    AXI_register_matrix_awvalid : out STD_LOGIC;
    AXI_register_matrix_bready : out STD_LOGIC;
    AXI_register_matrix_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_register_matrix_bvalid : in STD_LOGIC;
    AXI_register_matrix_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_register_matrix_rready : out STD_LOGIC;
    AXI_register_matrix_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_register_matrix_rvalid : in STD_LOGIC;
    AXI_register_matrix_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_register_matrix_wready : in STD_LOGIC;
    AXI_register_matrix_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_register_matrix_wvalid : out STD_LOGIC;
    KZ_BUS_CLK_N : in STD_LOGIC;
    KZ_BUS_CLK_P : in STD_LOGIC;
    KZ_CLK_OUT_N : out STD_LOGIC;
    KZ_CLK_OUT_P : out STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    iic_main_scl_io : inout STD_LOGIC;
    iic_main_sda_io : inout STD_LOGIC
  );
end IP_blob_wrapper;

architecture STRUCTURE of IP_blob_wrapper is
  component IP_blob is
  port (
    KZ_BUS_CLK_P : in STD_LOGIC;
    KZ_BUS_CLK_N : in STD_LOGIC;
    KZ_CLK_OUT_P : out STD_LOGIC;
    KZ_CLK_OUT_N : out STD_LOGIC;
    AXI_C2C_IN_P : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_IN_N : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_OUT_P : out STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_OUT_N : out STD_LOGIC_VECTOR ( 8 downto 0 );
    iic_main_scl_i : in STD_LOGIC;
    iic_main_scl_o : out STD_LOGIC;
    iic_main_scl_t : out STD_LOGIC;
    iic_main_sda_i : in STD_LOGIC;
    iic_main_sda_o : out STD_LOGIC;
    iic_main_sda_t : out STD_LOGIC;
    AXI_CLK200 : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    AXI_register_matrix_awaddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_register_matrix_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_register_matrix_awvalid : out STD_LOGIC;
    AXI_register_matrix_awready : in STD_LOGIC;
    AXI_register_matrix_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_register_matrix_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_register_matrix_wvalid : out STD_LOGIC;
    AXI_register_matrix_wready : in STD_LOGIC;
    AXI_register_matrix_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_register_matrix_bvalid : in STD_LOGIC;
    AXI_register_matrix_bready : out STD_LOGIC;
    AXI_register_matrix_araddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_register_matrix_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_register_matrix_arvalid : out STD_LOGIC;
    AXI_register_matrix_arready : in STD_LOGIC;
    AXI_register_matrix_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_register_matrix_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_register_matrix_rvalid : in STD_LOGIC;
    AXI_register_matrix_rready : out STD_LOGIC
  );
  end component IP_blob;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal iic_main_scl_i : STD_LOGIC;
  signal iic_main_scl_o : STD_LOGIC;
  signal iic_main_scl_t : STD_LOGIC;
  signal iic_main_sda_i : STD_LOGIC;
  signal iic_main_sda_o : STD_LOGIC;
  signal iic_main_sda_t : STD_LOGIC;
begin
IP_blob_i: component IP_blob
     port map (
      AXI_C2C_IN_N(8 downto 0) => AXI_C2C_IN_N(8 downto 0),
      AXI_C2C_IN_P(8 downto 0) => AXI_C2C_IN_P(8 downto 0),
      AXI_C2C_OUT_N(8 downto 0) => AXI_C2C_OUT_N(8 downto 0),
      AXI_C2C_OUT_P(8 downto 0) => AXI_C2C_OUT_P(8 downto 0),
      AXI_CLK200 => AXI_CLK200,
      AXI_register_matrix_araddr(30 downto 0) => AXI_register_matrix_araddr(30 downto 0),
      AXI_register_matrix_arprot(2 downto 0) => AXI_register_matrix_arprot(2 downto 0),
      AXI_register_matrix_arready => AXI_register_matrix_arready,
      AXI_register_matrix_arvalid => AXI_register_matrix_arvalid,
      AXI_register_matrix_awaddr(30 downto 0) => AXI_register_matrix_awaddr(30 downto 0),
      AXI_register_matrix_awprot(2 downto 0) => AXI_register_matrix_awprot(2 downto 0),
      AXI_register_matrix_awready => AXI_register_matrix_awready,
      AXI_register_matrix_awvalid => AXI_register_matrix_awvalid,
      AXI_register_matrix_bready => AXI_register_matrix_bready,
      AXI_register_matrix_bresp(1 downto 0) => AXI_register_matrix_bresp(1 downto 0),
      AXI_register_matrix_bvalid => AXI_register_matrix_bvalid,
      AXI_register_matrix_rdata(31 downto 0) => AXI_register_matrix_rdata(31 downto 0),
      AXI_register_matrix_rready => AXI_register_matrix_rready,
      AXI_register_matrix_rresp(1 downto 0) => AXI_register_matrix_rresp(1 downto 0),
      AXI_register_matrix_rvalid => AXI_register_matrix_rvalid,
      AXI_register_matrix_wdata(31 downto 0) => AXI_register_matrix_wdata(31 downto 0),
      AXI_register_matrix_wready => AXI_register_matrix_wready,
      AXI_register_matrix_wstrb(3 downto 0) => AXI_register_matrix_wstrb(3 downto 0),
      AXI_register_matrix_wvalid => AXI_register_matrix_wvalid,
      KZ_BUS_CLK_N => KZ_BUS_CLK_N,
      KZ_BUS_CLK_P => KZ_BUS_CLK_P,
      KZ_CLK_OUT_N => KZ_CLK_OUT_N,
      KZ_CLK_OUT_P => KZ_CLK_OUT_P,
      ext_reset_in => ext_reset_in,
      iic_main_scl_i => iic_main_scl_i,
      iic_main_scl_o => iic_main_scl_o,
      iic_main_scl_t => iic_main_scl_t,
      iic_main_sda_i => iic_main_sda_i,
      iic_main_sda_o => iic_main_sda_o,
      iic_main_sda_t => iic_main_sda_t
    );
iic_main_scl_iobuf: component IOBUF
     port map (
      I => iic_main_scl_o,
      IO => iic_main_scl_io,
      O => iic_main_scl_i,
      T => iic_main_scl_t
    );
iic_main_sda_iobuf: component IOBUF
     port map (
      I => iic_main_sda_o,
      IO => iic_main_sda_io,
      O => iic_main_sda_i,
      T => iic_main_sda_t
    );
end STRUCTURE;
