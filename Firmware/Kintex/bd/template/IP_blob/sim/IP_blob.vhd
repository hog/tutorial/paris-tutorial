--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Thu Nov 18 10:24:38 2021
--Host        : atlas126a running 64-bit openSUSE Tumbleweed
--Command     : generate_target IP_blob.bd
--Design      : IP_blob
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity IP_blob is
  port (
    AXI_C2C_IN_N : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_IN_P : in STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_OUT_N : out STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_C2C_OUT_P : out STD_LOGIC_VECTOR ( 8 downto 0 );
    AXI_CLK200 : in STD_LOGIC;
    AXI_register_matrix_araddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_register_matrix_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_register_matrix_arready : in STD_LOGIC;
    AXI_register_matrix_arvalid : out STD_LOGIC;
    AXI_register_matrix_awaddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_register_matrix_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_register_matrix_awready : in STD_LOGIC;
    AXI_register_matrix_awvalid : out STD_LOGIC;
    AXI_register_matrix_bready : out STD_LOGIC;
    AXI_register_matrix_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_register_matrix_bvalid : in STD_LOGIC;
    AXI_register_matrix_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_register_matrix_rready : out STD_LOGIC;
    AXI_register_matrix_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_register_matrix_rvalid : in STD_LOGIC;
    AXI_register_matrix_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_register_matrix_wready : in STD_LOGIC;
    AXI_register_matrix_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_register_matrix_wvalid : out STD_LOGIC;
    KZ_BUS_CLK_N : in STD_LOGIC;
    KZ_BUS_CLK_P : in STD_LOGIC;
    KZ_CLK_OUT_N : out STD_LOGIC;
    KZ_CLK_OUT_P : out STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    iic_main_scl_i : in STD_LOGIC;
    iic_main_scl_o : out STD_LOGIC;
    iic_main_scl_t : out STD_LOGIC;
    iic_main_sda_i : in STD_LOGIC;
    iic_main_sda_o : out STD_LOGIC;
    iic_main_sda_t : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of IP_blob : entity is "IP_blob,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=IP_blob,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=6,numReposBlks=6,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_board_cnt=1,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of IP_blob : entity is "IP_blob.hwdef";
end IP_blob;

architecture STRUCTURE of IP_blob is
  component IP_blob_axi_chip2chip_0_0 is
  port (
    m_aclk : in STD_LOGIC;
    m_aresetn : in STD_LOGIC;
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awready : in STD_LOGIC;
    m_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_wlast : out STD_LOGIC;
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wready : in STD_LOGIC;
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bready : out STD_LOGIC;
    m_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arvalid : out STD_LOGIC;
    m_axi_arready : in STD_LOGIC;
    m_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rlast : in STD_LOGIC;
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rready : out STD_LOGIC;
    axi_c2c_s2m_intr_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_c2c_m2s_intr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    idelay_ref_clk : in STD_LOGIC;
    axi_c2c_selio_tx_diff_clk_out_p : out STD_LOGIC;
    axi_c2c_selio_tx_diff_clk_out_n : out STD_LOGIC;
    axi_c2c_selio_tx_diff_data_out_p : out STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_selio_tx_diff_data_out_n : out STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_selio_rx_diff_clk_in_p : in STD_LOGIC;
    axi_c2c_selio_rx_diff_clk_in_n : in STD_LOGIC;
    axi_c2c_selio_rx_diff_data_in_p : in STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_selio_rx_diff_data_in_n : in STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_c2c_link_status_out : out STD_LOGIC;
    axi_c2c_multi_bit_error_out : out STD_LOGIC
  );
  end component IP_blob_axi_chip2chip_0_0;
  component IP_blob_xlconcat_0_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component IP_blob_xlconcat_0_0;
  component IP_blob_axi_iic_0_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    iic2intc_irpt : out STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 8 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 8 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    sda_i : in STD_LOGIC;
    sda_o : out STD_LOGIC;
    sda_t : out STD_LOGIC;
    scl_i : in STD_LOGIC;
    scl_o : out STD_LOGIC;
    scl_t : out STD_LOGIC;
    gpo : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component IP_blob_axi_iic_0_0;
  component IP_blob_proc_sys_reset_0_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component IP_blob_proc_sys_reset_0_0;
  component IP_blob_xadc_wiz_0_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    ip2intc_irpt : out STD_LOGIC;
    vp_in : in STD_LOGIC;
    vn_in : in STD_LOGIC;
    channel_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    eoc_out : out STD_LOGIC;
    alarm_out : out STD_LOGIC;
    eos_out : out STD_LOGIC;
    busy_out : out STD_LOGIC
  );
  end component IP_blob_xadc_wiz_0_0;
  component IP_blob_smartconnect_0_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    S00_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S00_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_awvalid : in STD_LOGIC;
    S00_AXI_awready : out STD_LOGIC;
    S00_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_wlast : in STD_LOGIC;
    S00_AXI_wvalid : in STD_LOGIC;
    S00_AXI_wready : out STD_LOGIC;
    S00_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_bvalid : out STD_LOGIC;
    S00_AXI_bready : in STD_LOGIC;
    S00_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S00_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    S00_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S00_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S00_AXI_arvalid : in STD_LOGIC;
    S00_AXI_arready : out STD_LOGIC;
    S00_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S00_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S00_AXI_rlast : out STD_LOGIC;
    S00_AXI_rvalid : out STD_LOGIC;
    S00_AXI_rready : in STD_LOGIC;
    M00_AXI_awaddr : out STD_LOGIC_VECTOR ( 8 downto 0 );
    M00_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_awvalid : out STD_LOGIC;
    M00_AXI_awready : in STD_LOGIC;
    M00_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_wvalid : out STD_LOGIC;
    M00_AXI_wready : in STD_LOGIC;
    M00_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_bvalid : in STD_LOGIC;
    M00_AXI_bready : out STD_LOGIC;
    M00_AXI_araddr : out STD_LOGIC_VECTOR ( 8 downto 0 );
    M00_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_arvalid : out STD_LOGIC;
    M00_AXI_arready : in STD_LOGIC;
    M00_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M00_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_rvalid : in STD_LOGIC;
    M00_AXI_rready : out STD_LOGIC;
    M01_AXI_awaddr : out STD_LOGIC_VECTOR ( 10 downto 0 );
    M01_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M01_AXI_awvalid : out STD_LOGIC;
    M01_AXI_awready : in STD_LOGIC;
    M01_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M01_AXI_wvalid : out STD_LOGIC;
    M01_AXI_wready : in STD_LOGIC;
    M01_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M01_AXI_bvalid : in STD_LOGIC;
    M01_AXI_bready : out STD_LOGIC;
    M01_AXI_araddr : out STD_LOGIC_VECTOR ( 10 downto 0 );
    M01_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M01_AXI_arvalid : out STD_LOGIC;
    M01_AXI_arready : in STD_LOGIC;
    M01_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M01_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M01_AXI_rvalid : in STD_LOGIC;
    M01_AXI_rready : out STD_LOGIC;
    M02_AXI_awaddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    M02_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M02_AXI_awvalid : out STD_LOGIC;
    M02_AXI_awready : in STD_LOGIC;
    M02_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M02_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M02_AXI_wvalid : out STD_LOGIC;
    M02_AXI_wready : in STD_LOGIC;
    M02_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M02_AXI_bvalid : in STD_LOGIC;
    M02_AXI_bready : out STD_LOGIC;
    M02_AXI_araddr : out STD_LOGIC_VECTOR ( 30 downto 0 );
    M02_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M02_AXI_arvalid : out STD_LOGIC;
    M02_AXI_arready : in STD_LOGIC;
    M02_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M02_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M02_AXI_rvalid : in STD_LOGIC;
    M02_AXI_rready : out STD_LOGIC
  );
  end component IP_blob_smartconnect_0_0;
  signal AXI_C2C_IN_N_1 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal AXI_C2C_IN_P_1 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal AXI_CLK200_1 : STD_LOGIC;
  signal KZ_BUS_CLK_N_1 : STD_LOGIC;
  signal KZ_BUS_CLK_P_1 : STD_LOGIC;
  signal axi_chip2chip_0_axi_c2c_selio_tx_diff_clk_out_n : STD_LOGIC;
  signal axi_chip2chip_0_axi_c2c_selio_tx_diff_clk_out_p : STD_LOGIC;
  signal axi_chip2chip_0_axi_c2c_selio_tx_diff_data_out_n : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal axi_chip2chip_0_axi_c2c_selio_tx_diff_data_out_p : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal axi_chip2chip_0_m_axi_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_ARLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal axi_chip2chip_0_m_axi_ARREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_chip2chip_0_m_axi_ARVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_AWLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal axi_chip2chip_0_m_axi_AWREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_chip2chip_0_m_axi_AWVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_BREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_BVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_RLAST : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_RREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_RVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_WLAST : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_WREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_chip2chip_0_m_axi_WVALID : STD_LOGIC;
  signal axi_iic_0_IIC_SCL_I : STD_LOGIC;
  signal axi_iic_0_IIC_SCL_O : STD_LOGIC;
  signal axi_iic_0_IIC_SCL_T : STD_LOGIC;
  signal axi_iic_0_IIC_SDA_I : STD_LOGIC;
  signal axi_iic_0_IIC_SDA_O : STD_LOGIC;
  signal axi_iic_0_IIC_SDA_T : STD_LOGIC;
  signal axi_iic_0_iic2intc_irpt : STD_LOGIC;
  signal ext_reset_in_1 : STD_LOGIC;
  signal proc_sys_reset_0_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal smartconnect_0_M00_AXI_ARADDR : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal smartconnect_0_M00_AXI_ARREADY : STD_LOGIC;
  signal smartconnect_0_M00_AXI_ARVALID : STD_LOGIC;
  signal smartconnect_0_M00_AXI_AWADDR : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal smartconnect_0_M00_AXI_AWREADY : STD_LOGIC;
  signal smartconnect_0_M00_AXI_AWVALID : STD_LOGIC;
  signal smartconnect_0_M00_AXI_BREADY : STD_LOGIC;
  signal smartconnect_0_M00_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal smartconnect_0_M00_AXI_BVALID : STD_LOGIC;
  signal smartconnect_0_M00_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal smartconnect_0_M00_AXI_RREADY : STD_LOGIC;
  signal smartconnect_0_M00_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal smartconnect_0_M00_AXI_RVALID : STD_LOGIC;
  signal smartconnect_0_M00_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal smartconnect_0_M00_AXI_WREADY : STD_LOGIC;
  signal smartconnect_0_M00_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal smartconnect_0_M00_AXI_WVALID : STD_LOGIC;
  signal smartconnect_0_M01_AXI_ARADDR : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal smartconnect_0_M01_AXI_ARREADY : STD_LOGIC;
  signal smartconnect_0_M01_AXI_ARVALID : STD_LOGIC;
  signal smartconnect_0_M01_AXI_AWADDR : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal smartconnect_0_M01_AXI_AWREADY : STD_LOGIC;
  signal smartconnect_0_M01_AXI_AWVALID : STD_LOGIC;
  signal smartconnect_0_M01_AXI_BREADY : STD_LOGIC;
  signal smartconnect_0_M01_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal smartconnect_0_M01_AXI_BVALID : STD_LOGIC;
  signal smartconnect_0_M01_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal smartconnect_0_M01_AXI_RREADY : STD_LOGIC;
  signal smartconnect_0_M01_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal smartconnect_0_M01_AXI_RVALID : STD_LOGIC;
  signal smartconnect_0_M01_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal smartconnect_0_M01_AXI_WREADY : STD_LOGIC;
  signal smartconnect_0_M01_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal smartconnect_0_M01_AXI_WVALID : STD_LOGIC;
  signal smartconnect_0_M02_AXI_ARADDR : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal smartconnect_0_M02_AXI_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal smartconnect_0_M02_AXI_ARREADY : STD_LOGIC;
  signal smartconnect_0_M02_AXI_ARVALID : STD_LOGIC;
  signal smartconnect_0_M02_AXI_AWADDR : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal smartconnect_0_M02_AXI_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal smartconnect_0_M02_AXI_AWREADY : STD_LOGIC;
  signal smartconnect_0_M02_AXI_AWVALID : STD_LOGIC;
  signal smartconnect_0_M02_AXI_BREADY : STD_LOGIC;
  signal smartconnect_0_M02_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal smartconnect_0_M02_AXI_BVALID : STD_LOGIC;
  signal smartconnect_0_M02_AXI_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal smartconnect_0_M02_AXI_RREADY : STD_LOGIC;
  signal smartconnect_0_M02_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal smartconnect_0_M02_AXI_RVALID : STD_LOGIC;
  signal smartconnect_0_M02_AXI_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal smartconnect_0_M02_AXI_WREADY : STD_LOGIC;
  signal smartconnect_0_M02_AXI_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal smartconnect_0_M02_AXI_WVALID : STD_LOGIC;
  signal xadc_wiz_0_ip2intc_irpt : STD_LOGIC;
  signal xlconcat_0_dout : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_axi_chip2chip_0_axi_c2c_link_status_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_axi_c2c_multi_bit_error_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_axi_c2c_m2s_intr_out_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_axi_iic_0_gpo_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_0_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_0_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_0_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_0_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_smartconnect_0_M00_AXI_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_smartconnect_0_M00_AXI_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_smartconnect_0_M01_AXI_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_smartconnect_0_M01_AXI_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_xadc_wiz_0_alarm_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_busy_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_eoc_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_eos_out_UNCONNECTED : STD_LOGIC;
  signal NLW_xadc_wiz_0_channel_out_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of AXI_CLK200 : signal is "xilinx.com:signal:clock:1.0 CLK.AXI_CLK200 CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of AXI_CLK200 : signal is "XIL_INTERFACENAME CLK.AXI_CLK200, ASSOCIATED_BUSIF AXI_register_matrix, CLK_DOMAIN IP_blob_AXI_CLK200, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of AXI_register_matrix_arready : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix ARREADY";
  attribute X_INTERFACE_INFO of AXI_register_matrix_arvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix ARVALID";
  attribute X_INTERFACE_INFO of AXI_register_matrix_awready : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix AWREADY";
  attribute X_INTERFACE_INFO of AXI_register_matrix_awvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix AWVALID";
  attribute X_INTERFACE_INFO of AXI_register_matrix_bready : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix BREADY";
  attribute X_INTERFACE_INFO of AXI_register_matrix_bvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix BVALID";
  attribute X_INTERFACE_INFO of AXI_register_matrix_rready : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix RREADY";
  attribute X_INTERFACE_INFO of AXI_register_matrix_rvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix RVALID";
  attribute X_INTERFACE_INFO of AXI_register_matrix_wready : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix WREADY";
  attribute X_INTERFACE_INFO of AXI_register_matrix_wvalid : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix WVALID";
  attribute X_INTERFACE_INFO of KZ_BUS_CLK_N : signal is "xilinx.com:signal:clock:1.0 CLK.KZ_BUS_CLK_N CLK";
  attribute X_INTERFACE_PARAMETER of KZ_BUS_CLK_N : signal is "XIL_INTERFACENAME CLK.KZ_BUS_CLK_N, CLK_DOMAIN IP_blob_KZ_BUS_CLK_N, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of KZ_BUS_CLK_P : signal is "xilinx.com:signal:clock:1.0 CLK.KZ_BUS_CLK_P CLK";
  attribute X_INTERFACE_PARAMETER of KZ_BUS_CLK_P : signal is "XIL_INTERFACENAME CLK.KZ_BUS_CLK_P, CLK_DOMAIN IP_blob_KZ_BUS_CLK_P, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of KZ_CLK_OUT_N : signal is "xilinx.com:signal:clock:1.0 CLK.KZ_CLK_OUT_N CLK";
  attribute X_INTERFACE_PARAMETER of KZ_CLK_OUT_N : signal is "XIL_INTERFACENAME CLK.KZ_CLK_OUT_N, CLK_DOMAIN IP_blob_axi_chip2chip_0_0_axi_c2c_selio_tx_diff_clk_out_n, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of KZ_CLK_OUT_P : signal is "xilinx.com:signal:clock:1.0 CLK.KZ_CLK_OUT_P CLK";
  attribute X_INTERFACE_PARAMETER of KZ_CLK_OUT_P : signal is "XIL_INTERFACENAME CLK.KZ_CLK_OUT_P, CLK_DOMAIN IP_blob_axi_chip2chip_0_0_axi_c2c_selio_tx_diff_clk_out_p, FREQ_HZ 200000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of ext_reset_in : signal is "xilinx.com:signal:reset:1.0 RST.EXT_RESET_IN RST";
  attribute X_INTERFACE_PARAMETER of ext_reset_in : signal is "XIL_INTERFACENAME RST.EXT_RESET_IN, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of iic_main_scl_i : signal is "xilinx.com:interface:iic:1.0 iic_main SCL_I";
  attribute X_INTERFACE_INFO of iic_main_scl_o : signal is "xilinx.com:interface:iic:1.0 iic_main SCL_O";
  attribute X_INTERFACE_INFO of iic_main_scl_t : signal is "xilinx.com:interface:iic:1.0 iic_main SCL_T";
  attribute X_INTERFACE_INFO of iic_main_sda_i : signal is "xilinx.com:interface:iic:1.0 iic_main SDA_I";
  attribute X_INTERFACE_INFO of iic_main_sda_o : signal is "xilinx.com:interface:iic:1.0 iic_main SDA_O";
  attribute X_INTERFACE_INFO of iic_main_sda_t : signal is "xilinx.com:interface:iic:1.0 iic_main SDA_T";
  attribute X_INTERFACE_INFO of AXI_register_matrix_araddr : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix ARADDR";
  attribute X_INTERFACE_PARAMETER of AXI_register_matrix_araddr : signal is "XIL_INTERFACENAME AXI_register_matrix, ADDR_WIDTH 31, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN IP_blob_AXI_CLK200, DATA_WIDTH 32, FREQ_HZ 200000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 16, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 16, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of AXI_register_matrix_arprot : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix ARPROT";
  attribute X_INTERFACE_INFO of AXI_register_matrix_awaddr : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix AWADDR";
  attribute X_INTERFACE_INFO of AXI_register_matrix_awprot : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix AWPROT";
  attribute X_INTERFACE_INFO of AXI_register_matrix_bresp : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix BRESP";
  attribute X_INTERFACE_INFO of AXI_register_matrix_rdata : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix RDATA";
  attribute X_INTERFACE_INFO of AXI_register_matrix_rresp : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix RRESP";
  attribute X_INTERFACE_INFO of AXI_register_matrix_wdata : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix WDATA";
  attribute X_INTERFACE_INFO of AXI_register_matrix_wstrb : signal is "xilinx.com:interface:aximm:1.0 AXI_register_matrix WSTRB";
begin
  AXI_C2C_IN_N_1(8 downto 0) <= AXI_C2C_IN_N(8 downto 0);
  AXI_C2C_IN_P_1(8 downto 0) <= AXI_C2C_IN_P(8 downto 0);
  AXI_C2C_OUT_N(8 downto 0) <= axi_chip2chip_0_axi_c2c_selio_tx_diff_data_out_n(8 downto 0);
  AXI_C2C_OUT_P(8 downto 0) <= axi_chip2chip_0_axi_c2c_selio_tx_diff_data_out_p(8 downto 0);
  AXI_CLK200_1 <= AXI_CLK200;
  AXI_register_matrix_araddr(30 downto 0) <= smartconnect_0_M02_AXI_ARADDR(30 downto 0);
  AXI_register_matrix_arprot(2 downto 0) <= smartconnect_0_M02_AXI_ARPROT(2 downto 0);
  AXI_register_matrix_arvalid <= smartconnect_0_M02_AXI_ARVALID;
  AXI_register_matrix_awaddr(30 downto 0) <= smartconnect_0_M02_AXI_AWADDR(30 downto 0);
  AXI_register_matrix_awprot(2 downto 0) <= smartconnect_0_M02_AXI_AWPROT(2 downto 0);
  AXI_register_matrix_awvalid <= smartconnect_0_M02_AXI_AWVALID;
  AXI_register_matrix_bready <= smartconnect_0_M02_AXI_BREADY;
  AXI_register_matrix_rready <= smartconnect_0_M02_AXI_RREADY;
  AXI_register_matrix_wdata(31 downto 0) <= smartconnect_0_M02_AXI_WDATA(31 downto 0);
  AXI_register_matrix_wstrb(3 downto 0) <= smartconnect_0_M02_AXI_WSTRB(3 downto 0);
  AXI_register_matrix_wvalid <= smartconnect_0_M02_AXI_WVALID;
  KZ_BUS_CLK_N_1 <= KZ_BUS_CLK_N;
  KZ_BUS_CLK_P_1 <= KZ_BUS_CLK_P;
  KZ_CLK_OUT_N <= axi_chip2chip_0_axi_c2c_selio_tx_diff_clk_out_n;
  KZ_CLK_OUT_P <= axi_chip2chip_0_axi_c2c_selio_tx_diff_clk_out_p;
  axi_iic_0_IIC_SCL_I <= iic_main_scl_i;
  axi_iic_0_IIC_SDA_I <= iic_main_sda_i;
  ext_reset_in_1 <= ext_reset_in;
  iic_main_scl_o <= axi_iic_0_IIC_SCL_O;
  iic_main_scl_t <= axi_iic_0_IIC_SCL_T;
  iic_main_sda_o <= axi_iic_0_IIC_SDA_O;
  iic_main_sda_t <= axi_iic_0_IIC_SDA_T;
  smartconnect_0_M02_AXI_ARREADY <= AXI_register_matrix_arready;
  smartconnect_0_M02_AXI_AWREADY <= AXI_register_matrix_awready;
  smartconnect_0_M02_AXI_BRESP(1 downto 0) <= AXI_register_matrix_bresp(1 downto 0);
  smartconnect_0_M02_AXI_BVALID <= AXI_register_matrix_bvalid;
  smartconnect_0_M02_AXI_RDATA(31 downto 0) <= AXI_register_matrix_rdata(31 downto 0);
  smartconnect_0_M02_AXI_RRESP(1 downto 0) <= AXI_register_matrix_rresp(1 downto 0);
  smartconnect_0_M02_AXI_RVALID <= AXI_register_matrix_rvalid;
  smartconnect_0_M02_AXI_WREADY <= AXI_register_matrix_wready;
axi_chip2chip_0: component IP_blob_axi_chip2chip_0_0
     port map (
      axi_c2c_link_status_out => NLW_axi_chip2chip_0_axi_c2c_link_status_out_UNCONNECTED,
      axi_c2c_m2s_intr_out(3 downto 0) => NLW_axi_chip2chip_0_axi_c2c_m2s_intr_out_UNCONNECTED(3 downto 0),
      axi_c2c_multi_bit_error_out => NLW_axi_chip2chip_0_axi_c2c_multi_bit_error_out_UNCONNECTED,
      axi_c2c_s2m_intr_in(3 downto 0) => xlconcat_0_dout(3 downto 0),
      axi_c2c_selio_rx_diff_clk_in_n => KZ_BUS_CLK_N_1,
      axi_c2c_selio_rx_diff_clk_in_p => KZ_BUS_CLK_P_1,
      axi_c2c_selio_rx_diff_data_in_n(8 downto 0) => AXI_C2C_IN_N_1(8 downto 0),
      axi_c2c_selio_rx_diff_data_in_p(8 downto 0) => AXI_C2C_IN_P_1(8 downto 0),
      axi_c2c_selio_tx_diff_clk_out_n => axi_chip2chip_0_axi_c2c_selio_tx_diff_clk_out_n,
      axi_c2c_selio_tx_diff_clk_out_p => axi_chip2chip_0_axi_c2c_selio_tx_diff_clk_out_p,
      axi_c2c_selio_tx_diff_data_out_n(8 downto 0) => axi_chip2chip_0_axi_c2c_selio_tx_diff_data_out_n(8 downto 0),
      axi_c2c_selio_tx_diff_data_out_p(8 downto 0) => axi_chip2chip_0_axi_c2c_selio_tx_diff_data_out_p(8 downto 0),
      idelay_ref_clk => AXI_CLK200_1,
      m_aclk => AXI_CLK200_1,
      m_aresetn => proc_sys_reset_0_peripheral_aresetn(0),
      m_axi_araddr(31 downto 0) => axi_chip2chip_0_m_axi_ARADDR(31 downto 0),
      m_axi_arburst(1 downto 0) => axi_chip2chip_0_m_axi_ARBURST(1 downto 0),
      m_axi_arlen(7 downto 0) => axi_chip2chip_0_m_axi_ARLEN(7 downto 0),
      m_axi_arready => axi_chip2chip_0_m_axi_ARREADY,
      m_axi_arsize(2 downto 0) => axi_chip2chip_0_m_axi_ARSIZE(2 downto 0),
      m_axi_arvalid => axi_chip2chip_0_m_axi_ARVALID,
      m_axi_awaddr(31 downto 0) => axi_chip2chip_0_m_axi_AWADDR(31 downto 0),
      m_axi_awburst(1 downto 0) => axi_chip2chip_0_m_axi_AWBURST(1 downto 0),
      m_axi_awlen(7 downto 0) => axi_chip2chip_0_m_axi_AWLEN(7 downto 0),
      m_axi_awready => axi_chip2chip_0_m_axi_AWREADY,
      m_axi_awsize(2 downto 0) => axi_chip2chip_0_m_axi_AWSIZE(2 downto 0),
      m_axi_awvalid => axi_chip2chip_0_m_axi_AWVALID,
      m_axi_bready => axi_chip2chip_0_m_axi_BREADY,
      m_axi_bresp(1 downto 0) => axi_chip2chip_0_m_axi_BRESP(1 downto 0),
      m_axi_bvalid => axi_chip2chip_0_m_axi_BVALID,
      m_axi_rdata(31 downto 0) => axi_chip2chip_0_m_axi_RDATA(31 downto 0),
      m_axi_rlast => axi_chip2chip_0_m_axi_RLAST,
      m_axi_rready => axi_chip2chip_0_m_axi_RREADY,
      m_axi_rresp(1 downto 0) => axi_chip2chip_0_m_axi_RRESP(1 downto 0),
      m_axi_rvalid => axi_chip2chip_0_m_axi_RVALID,
      m_axi_wdata(31 downto 0) => axi_chip2chip_0_m_axi_WDATA(31 downto 0),
      m_axi_wlast => axi_chip2chip_0_m_axi_WLAST,
      m_axi_wready => axi_chip2chip_0_m_axi_WREADY,
      m_axi_wstrb(3 downto 0) => axi_chip2chip_0_m_axi_WSTRB(3 downto 0),
      m_axi_wvalid => axi_chip2chip_0_m_axi_WVALID
    );
axi_iic_0: component IP_blob_axi_iic_0_0
     port map (
      gpo(0) => NLW_axi_iic_0_gpo_UNCONNECTED(0),
      iic2intc_irpt => axi_iic_0_iic2intc_irpt,
      s_axi_aclk => AXI_CLK200_1,
      s_axi_araddr(8 downto 0) => smartconnect_0_M00_AXI_ARADDR(8 downto 0),
      s_axi_aresetn => proc_sys_reset_0_peripheral_aresetn(0),
      s_axi_arready => smartconnect_0_M00_AXI_ARREADY,
      s_axi_arvalid => smartconnect_0_M00_AXI_ARVALID,
      s_axi_awaddr(8 downto 0) => smartconnect_0_M00_AXI_AWADDR(8 downto 0),
      s_axi_awready => smartconnect_0_M00_AXI_AWREADY,
      s_axi_awvalid => smartconnect_0_M00_AXI_AWVALID,
      s_axi_bready => smartconnect_0_M00_AXI_BREADY,
      s_axi_bresp(1 downto 0) => smartconnect_0_M00_AXI_BRESP(1 downto 0),
      s_axi_bvalid => smartconnect_0_M00_AXI_BVALID,
      s_axi_rdata(31 downto 0) => smartconnect_0_M00_AXI_RDATA(31 downto 0),
      s_axi_rready => smartconnect_0_M00_AXI_RREADY,
      s_axi_rresp(1 downto 0) => smartconnect_0_M00_AXI_RRESP(1 downto 0),
      s_axi_rvalid => smartconnect_0_M00_AXI_RVALID,
      s_axi_wdata(31 downto 0) => smartconnect_0_M00_AXI_WDATA(31 downto 0),
      s_axi_wready => smartconnect_0_M00_AXI_WREADY,
      s_axi_wstrb(3 downto 0) => smartconnect_0_M00_AXI_WSTRB(3 downto 0),
      s_axi_wvalid => smartconnect_0_M00_AXI_WVALID,
      scl_i => axi_iic_0_IIC_SCL_I,
      scl_o => axi_iic_0_IIC_SCL_O,
      scl_t => axi_iic_0_IIC_SCL_T,
      sda_i => axi_iic_0_IIC_SDA_I,
      sda_o => axi_iic_0_IIC_SDA_O,
      sda_t => axi_iic_0_IIC_SDA_T
    );
proc_sys_reset_0: component IP_blob_proc_sys_reset_0_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_proc_sys_reset_0_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => ext_reset_in_1,
      interconnect_aresetn(0) => NLW_proc_sys_reset_0_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_proc_sys_reset_0_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => proc_sys_reset_0_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_proc_sys_reset_0_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => AXI_CLK200_1
    );
smartconnect_0: component IP_blob_smartconnect_0_0
     port map (
      M00_AXI_araddr(8 downto 0) => smartconnect_0_M00_AXI_ARADDR(8 downto 0),
      M00_AXI_arprot(2 downto 0) => NLW_smartconnect_0_M00_AXI_arprot_UNCONNECTED(2 downto 0),
      M00_AXI_arready => smartconnect_0_M00_AXI_ARREADY,
      M00_AXI_arvalid => smartconnect_0_M00_AXI_ARVALID,
      M00_AXI_awaddr(8 downto 0) => smartconnect_0_M00_AXI_AWADDR(8 downto 0),
      M00_AXI_awprot(2 downto 0) => NLW_smartconnect_0_M00_AXI_awprot_UNCONNECTED(2 downto 0),
      M00_AXI_awready => smartconnect_0_M00_AXI_AWREADY,
      M00_AXI_awvalid => smartconnect_0_M00_AXI_AWVALID,
      M00_AXI_bready => smartconnect_0_M00_AXI_BREADY,
      M00_AXI_bresp(1 downto 0) => smartconnect_0_M00_AXI_BRESP(1 downto 0),
      M00_AXI_bvalid => smartconnect_0_M00_AXI_BVALID,
      M00_AXI_rdata(31 downto 0) => smartconnect_0_M00_AXI_RDATA(31 downto 0),
      M00_AXI_rready => smartconnect_0_M00_AXI_RREADY,
      M00_AXI_rresp(1 downto 0) => smartconnect_0_M00_AXI_RRESP(1 downto 0),
      M00_AXI_rvalid => smartconnect_0_M00_AXI_RVALID,
      M00_AXI_wdata(31 downto 0) => smartconnect_0_M00_AXI_WDATA(31 downto 0),
      M00_AXI_wready => smartconnect_0_M00_AXI_WREADY,
      M00_AXI_wstrb(3 downto 0) => smartconnect_0_M00_AXI_WSTRB(3 downto 0),
      M00_AXI_wvalid => smartconnect_0_M00_AXI_WVALID,
      M01_AXI_araddr(10 downto 0) => smartconnect_0_M01_AXI_ARADDR(10 downto 0),
      M01_AXI_arprot(2 downto 0) => NLW_smartconnect_0_M01_AXI_arprot_UNCONNECTED(2 downto 0),
      M01_AXI_arready => smartconnect_0_M01_AXI_ARREADY,
      M01_AXI_arvalid => smartconnect_0_M01_AXI_ARVALID,
      M01_AXI_awaddr(10 downto 0) => smartconnect_0_M01_AXI_AWADDR(10 downto 0),
      M01_AXI_awprot(2 downto 0) => NLW_smartconnect_0_M01_AXI_awprot_UNCONNECTED(2 downto 0),
      M01_AXI_awready => smartconnect_0_M01_AXI_AWREADY,
      M01_AXI_awvalid => smartconnect_0_M01_AXI_AWVALID,
      M01_AXI_bready => smartconnect_0_M01_AXI_BREADY,
      M01_AXI_bresp(1 downto 0) => smartconnect_0_M01_AXI_BRESP(1 downto 0),
      M01_AXI_bvalid => smartconnect_0_M01_AXI_BVALID,
      M01_AXI_rdata(31 downto 0) => smartconnect_0_M01_AXI_RDATA(31 downto 0),
      M01_AXI_rready => smartconnect_0_M01_AXI_RREADY,
      M01_AXI_rresp(1 downto 0) => smartconnect_0_M01_AXI_RRESP(1 downto 0),
      M01_AXI_rvalid => smartconnect_0_M01_AXI_RVALID,
      M01_AXI_wdata(31 downto 0) => smartconnect_0_M01_AXI_WDATA(31 downto 0),
      M01_AXI_wready => smartconnect_0_M01_AXI_WREADY,
      M01_AXI_wstrb(3 downto 0) => smartconnect_0_M01_AXI_WSTRB(3 downto 0),
      M01_AXI_wvalid => smartconnect_0_M01_AXI_WVALID,
      M02_AXI_araddr(30 downto 0) => smartconnect_0_M02_AXI_ARADDR(30 downto 0),
      M02_AXI_arprot(2 downto 0) => smartconnect_0_M02_AXI_ARPROT(2 downto 0),
      M02_AXI_arready => smartconnect_0_M02_AXI_ARREADY,
      M02_AXI_arvalid => smartconnect_0_M02_AXI_ARVALID,
      M02_AXI_awaddr(30 downto 0) => smartconnect_0_M02_AXI_AWADDR(30 downto 0),
      M02_AXI_awprot(2 downto 0) => smartconnect_0_M02_AXI_AWPROT(2 downto 0),
      M02_AXI_awready => smartconnect_0_M02_AXI_AWREADY,
      M02_AXI_awvalid => smartconnect_0_M02_AXI_AWVALID,
      M02_AXI_bready => smartconnect_0_M02_AXI_BREADY,
      M02_AXI_bresp(1 downto 0) => smartconnect_0_M02_AXI_BRESP(1 downto 0),
      M02_AXI_bvalid => smartconnect_0_M02_AXI_BVALID,
      M02_AXI_rdata(31 downto 0) => smartconnect_0_M02_AXI_RDATA(31 downto 0),
      M02_AXI_rready => smartconnect_0_M02_AXI_RREADY,
      M02_AXI_rresp(1 downto 0) => smartconnect_0_M02_AXI_RRESP(1 downto 0),
      M02_AXI_rvalid => smartconnect_0_M02_AXI_RVALID,
      M02_AXI_wdata(31 downto 0) => smartconnect_0_M02_AXI_WDATA(31 downto 0),
      M02_AXI_wready => smartconnect_0_M02_AXI_WREADY,
      M02_AXI_wstrb(3 downto 0) => smartconnect_0_M02_AXI_WSTRB(3 downto 0),
      M02_AXI_wvalid => smartconnect_0_M02_AXI_WVALID,
      S00_AXI_araddr(31 downto 0) => axi_chip2chip_0_m_axi_ARADDR(31 downto 0),
      S00_AXI_arburst(1 downto 0) => axi_chip2chip_0_m_axi_ARBURST(1 downto 0),
      S00_AXI_arcache(3 downto 0) => B"0011",
      S00_AXI_arlen(7 downto 0) => axi_chip2chip_0_m_axi_ARLEN(7 downto 0),
      S00_AXI_arlock(0) => '0',
      S00_AXI_arprot(2 downto 0) => B"000",
      S00_AXI_arqos(3 downto 0) => B"0000",
      S00_AXI_arready => axi_chip2chip_0_m_axi_ARREADY,
      S00_AXI_arsize(2 downto 0) => axi_chip2chip_0_m_axi_ARSIZE(2 downto 0),
      S00_AXI_arvalid => axi_chip2chip_0_m_axi_ARVALID,
      S00_AXI_awaddr(31 downto 0) => axi_chip2chip_0_m_axi_AWADDR(31 downto 0),
      S00_AXI_awburst(1 downto 0) => axi_chip2chip_0_m_axi_AWBURST(1 downto 0),
      S00_AXI_awcache(3 downto 0) => B"0011",
      S00_AXI_awlen(7 downto 0) => axi_chip2chip_0_m_axi_AWLEN(7 downto 0),
      S00_AXI_awlock(0) => '0',
      S00_AXI_awprot(2 downto 0) => B"000",
      S00_AXI_awqos(3 downto 0) => B"0000",
      S00_AXI_awready => axi_chip2chip_0_m_axi_AWREADY,
      S00_AXI_awsize(2 downto 0) => axi_chip2chip_0_m_axi_AWSIZE(2 downto 0),
      S00_AXI_awvalid => axi_chip2chip_0_m_axi_AWVALID,
      S00_AXI_bready => axi_chip2chip_0_m_axi_BREADY,
      S00_AXI_bresp(1 downto 0) => axi_chip2chip_0_m_axi_BRESP(1 downto 0),
      S00_AXI_bvalid => axi_chip2chip_0_m_axi_BVALID,
      S00_AXI_rdata(31 downto 0) => axi_chip2chip_0_m_axi_RDATA(31 downto 0),
      S00_AXI_rlast => axi_chip2chip_0_m_axi_RLAST,
      S00_AXI_rready => axi_chip2chip_0_m_axi_RREADY,
      S00_AXI_rresp(1 downto 0) => axi_chip2chip_0_m_axi_RRESP(1 downto 0),
      S00_AXI_rvalid => axi_chip2chip_0_m_axi_RVALID,
      S00_AXI_wdata(31 downto 0) => axi_chip2chip_0_m_axi_WDATA(31 downto 0),
      S00_AXI_wlast => axi_chip2chip_0_m_axi_WLAST,
      S00_AXI_wready => axi_chip2chip_0_m_axi_WREADY,
      S00_AXI_wstrb(3 downto 0) => axi_chip2chip_0_m_axi_WSTRB(3 downto 0),
      S00_AXI_wvalid => axi_chip2chip_0_m_axi_WVALID,
      aclk => AXI_CLK200_1,
      aresetn => proc_sys_reset_0_peripheral_aresetn(0)
    );
xadc_wiz_0: component IP_blob_xadc_wiz_0_0
     port map (
      alarm_out => NLW_xadc_wiz_0_alarm_out_UNCONNECTED,
      busy_out => NLW_xadc_wiz_0_busy_out_UNCONNECTED,
      channel_out(4 downto 0) => NLW_xadc_wiz_0_channel_out_UNCONNECTED(4 downto 0),
      eoc_out => NLW_xadc_wiz_0_eoc_out_UNCONNECTED,
      eos_out => NLW_xadc_wiz_0_eos_out_UNCONNECTED,
      ip2intc_irpt => xadc_wiz_0_ip2intc_irpt,
      s_axi_aclk => AXI_CLK200_1,
      s_axi_araddr(10 downto 0) => smartconnect_0_M01_AXI_ARADDR(10 downto 0),
      s_axi_aresetn => proc_sys_reset_0_peripheral_aresetn(0),
      s_axi_arready => smartconnect_0_M01_AXI_ARREADY,
      s_axi_arvalid => smartconnect_0_M01_AXI_ARVALID,
      s_axi_awaddr(10 downto 0) => smartconnect_0_M01_AXI_AWADDR(10 downto 0),
      s_axi_awready => smartconnect_0_M01_AXI_AWREADY,
      s_axi_awvalid => smartconnect_0_M01_AXI_AWVALID,
      s_axi_bready => smartconnect_0_M01_AXI_BREADY,
      s_axi_bresp(1 downto 0) => smartconnect_0_M01_AXI_BRESP(1 downto 0),
      s_axi_bvalid => smartconnect_0_M01_AXI_BVALID,
      s_axi_rdata(31 downto 0) => smartconnect_0_M01_AXI_RDATA(31 downto 0),
      s_axi_rready => smartconnect_0_M01_AXI_RREADY,
      s_axi_rresp(1 downto 0) => smartconnect_0_M01_AXI_RRESP(1 downto 0),
      s_axi_rvalid => smartconnect_0_M01_AXI_RVALID,
      s_axi_wdata(31 downto 0) => smartconnect_0_M01_AXI_WDATA(31 downto 0),
      s_axi_wready => smartconnect_0_M01_AXI_WREADY,
      s_axi_wstrb(3 downto 0) => smartconnect_0_M01_AXI_WSTRB(3 downto 0),
      s_axi_wvalid => smartconnect_0_M01_AXI_WVALID,
      vn_in => '0',
      vp_in => '0'
    );
xlconcat_0: component IP_blob_xlconcat_0_0
     port map (
      In0(0) => axi_iic_0_iic2intc_irpt,
      In1(0) => xadc_wiz_0_ip2intc_irpt,
      In2(0) => '0',
      In3(0) => '0',
      dout(3 downto 0) => xlconcat_0_dout(3 downto 0)
    );
end STRUCTURE;
