################################################################################

# This XDC is used only for OOC mode of synthesis, implementation
# This constraints file contains default clock frequencies to be used during
# out-of-context flows such as OOC Synthesis and Hierarchical Designs.
# This constraints file is not used in normal top-down synthesis (default flow
# of Vivado)
################################################################################
create_clock -name KZ_BUS_CLK_P -period 5 [get_ports KZ_BUS_CLK_P]
create_clock -name KZ_BUS_CLK_N -period 5 [get_ports KZ_BUS_CLK_N]
create_clock -name AXI_CLK200 -period 5 [get_ports AXI_CLK200]

################################################################################