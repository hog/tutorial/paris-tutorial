----------------------------------------------------------------------------------
-- Company: INFN & University of Bologna
-- Developer: Nico Giangiacomi 
-- 
-- Create Date: 04/02/2018 01:58:37 PM
-- Design Name: Protocol_Converter
-- Module Name: Aurora_to_FM_wrapper - Behavioral
-- Project Name: Protocol_Converter
-- Target Devices: Bologna INFN PiLUP Board
-- Tool Versions: 1.0
-- Description: This design is in charge of merging 4 lanes RD53A data (from Aurora)
-- in a single FM link
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- data_i, data_o (33 downto 32) meaning:   00 ---> header
--                                          01 ---> data
--                                          10 ---> AURORA code 
--                                          11 ---> Register frame
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.FMTransceiverPackage.all;


entity Aurora_to_FM_wrapper is
Generic (
	empty_timeout	: integer:= 10
);
port (
	--reset
	rst				: in std_logic;
	--clock signals
	clk160			: in std_logic;
	clk240_rd		: in std_logic;
	
	-- input signals
	RD53_data		: in slv64_array(3 downto 0);
	RD53_header		: in std_logic_vector(1 downto 0);
	
	-- output signals
	FM_data			: out std_logic_vector(31 downto 0);
	FM_data_type	: out std_logic_vector(1 downto 0);
	FM_valid		: out std_logic
);
end Aurora_to_FM_wrapper;

architecture Behavioral of Aurora_to_FM_wrapper is
----------------------------------------------------------
--                                                      --
--                  Signal Declaration                  --
--                                                      --
----------------------------------------------------------
	signal RD53_data_i, RD53_data_ii, RD53_data_iii	: std_logic_vector(257 downto 0);
	signal data_i, data_o							: std_logic_vector(33 downto 0);
	signal data_wr, data_rd							: std_logic;
	signal data_valid								: std_logic;
	signal fifo_full, fifo_empty					: std_logic;
	
	signal lane										: integer range 0 to 3:= 0;
	signal first_word								: std_logic;

	type state_type 								is (idle, read_en, start_packet, send_data, send_reg, send_code, close_packet, send_idle);
	signal state									: state_type := idle;
	
	signal store_data								: std_logic_vector(33 downto 0);
	signal store_data_valid							: std_logic;
	
	signal empty_cnt								: integer:= 0;
	
----------------------------------------------------------
--                                                      --
--                  Module Declaration                  --
--                                                      --
----------------------------------------------------------
	COMPONENT fifo_34
	PORT (
		rst 		: IN STD_LOGIC;
		wr_clk		: IN STD_LOGIC;
		rd_clk 		: IN STD_LOGIC;
		din 		: IN STD_LOGIC_VECTOR(33 DOWNTO 0);
		wr_en 		: IN STD_LOGIC;
		rd_en 		: IN STD_LOGIC;
		dout 		: OUT STD_LOGIC_VECTOR(33 DOWNTO 0);
		full 		: OUT STD_LOGIC;
		empty 		: OUT STD_LOGIC;
		valid 		: OUT STD_LOGIC
	);
	END COMPONENT;


begin
	RD53_data_i			<= RD53_header & RD53_data(3) & RD53_data(2) & RD53_data(1) & RD53_data(0);
----------------------------------------------------------
--                                                      --
--                 Module Implementation                --
--                                                      --
----------------------------------------------------------
	RD53_store_fifo: fifo_34
	Port Map (
		rst				=> rst,
		wr_clk			=> clk160,
		rd_clk			=> clk240_rd,
		din				=> data_i,
		wr_en			=> data_wr,
		rd_en			=> data_rd,
		dout			=> data_o,
		full			=> fifo_full,
		empty			=> fifo_empty,
		valid			=> data_valid
	);

----------------------------------------------------------
--                                                      --
--                Process Implementation                --
--                                                      --
----------------------------------------------------------

	fill_data_fifo: process(rst, clk160)
	begin
		if(rst = '1') then
			lane			<= 0;
			first_word		<= '1';
			data_wr			<= '0';
			RD53_data_ii	<= (others => '0');
			RD53_data_iii	<= (others => '0');
		elsif(rising_edge(clk160)) then
			RD53_data_ii		<= RD53_data_i;
			RD53_data_iii		<= RD53_data_i;
			
			data_wr				<= '0';
			first_word			<= not first_word;
			if(RD53_data_ii /= RD53_data_i) then
				lane		<= 0;
				first_word	<= '1';
			elsif(first_word = '0') then
				lane		<= lane + 1;
				if(lane = 3) then
					lane		<= 0;
				end if;
			end if;
			
			if(first_word = '1') then
				if(RD53_data_iii(257 downto 256) = "01") then -- data
					if(RD53_data_iii(63 + 64*lane downto 57 + 64*lane) = "0000001") then -- header
						data_i			<= "00" & RD53_data_iii(63 + 64*lane downto 32 + 64*lane);
						data_wr			<= '1';
					else	-- hit
						data_i			<= "01" & RD53_data_iii(63 + 64*lane downto 32 + 64*lane);
						data_wr			<= '1';
					end if;
				elsif(RD53_data_iii(63 + 64*lane downto 56 + 64*lane) = x"78") then -- aurora code
					data_i			<= "10" & RD53_data_iii(63 + 64*lane downto 32 + 64*lane);
					data_wr			<= '1';
				elsif(RD53_data_iii(63 + 64*lane downto 56 + 64*lane) /= x"1E") then -- register frame
					data_i			<= "11" & RD53_data_iii(63 + 64*lane downto 32 + 64*lane);
					data_wr			<= '1';
				end if;
			else -- second word
				if(RD53_data_iii(257 downto 256) = "01") or (RD53_data_iii(63 + 64*lane downto 48 + 64*lane) = x"1E04") then -- data
					if(RD53_data_iii(31 + 64*lane downto 25 + 64*lane) = "0000001") then -- header
						data_i			<= "00" & RD53_data_iii(31 + 64*lane downto 64*lane);
						data_wr			<= '1';
					else	-- hit
						data_i			<= "01" & RD53_data_iii(31 + 64*lane downto 64*lane);
						data_wr			<= '1';
					end if;
				elsif(RD53_data_iii(63 + 64*lane downto 56 + 64*lane) /= x"1E") and (RD53_data_iii(63 + 64*lane downto 56 + 64*lane) /= x"78") then -- register frame
					data_i			<= "11" & RD53_data_iii(31 + 64*lane downto 64*lane);
					data_wr			<= '1';
				end if;
			end if;
		end if;
	end process fill_data_fifo;
	
	
	FM_data_controller: process(rst, clk240_rd)
	begin
		if(rst = '1') then
			data_rd				<= '0';
			FM_data				<= (others => '0');
			FM_data_type		<= "11";
			FM_valid			<= '0';
			store_data_valid	<= '0';
			store_data			<= (others => '0');
			empty_cnt			<= 0;
			state				<= idle;
		elsif(rising_edge(clk240_rd)) then
			data_rd			<= '0';
			if(fifo_empty = '1') and (empty_cnt < empty_timeout) then
				empty_cnt		<= empty_cnt + 1;
			elsif(fifo_empty = '0') then
				empty_cnt		<= 0;
			end if;
			case state is
				when idle =>
					FM_valid			<= '0';
					FM_data				<= (others => '0');
					FM_data_type		<= "11";
					if(fifo_empty = '0') then
						state			<= read_en;
					end if;
				
				when read_en =>
					data_rd				<= '1';
					FM_valid			<= '1';
					FM_data				<= x"00000000";
					FM_data_type		<= "11";
					store_data			<= data_o;
					state				<= start_packet;
					
				when send_idle =>
					data_rd				<= '0';
					FM_valid			<= '1';
					FM_data				<= x"00000000";
					FM_data_type		<= "11";
					state				<= start_packet;	
				
				when start_packet =>
					FM_valid			<= '1';
					data_rd				<= '1';
					if(store_data(33 downto 32) = "00") then --start new data with header
						FM_data				<= x"BDF00000";
						FM_data_type		<= "01";
						state				<= send_data;
					elsif(store_data(33 downto 32) = "01") then --continuing previous packet
						FM_data				<= x"CDF00000";
						FM_data_type		<= "01";
						state				<= send_data;
					elsif(store_data(33 downto 32) = "01") then --sending Aurora frame packet
						FM_data				<= x"AF000000";
						FM_data_type		<= "01";
						state				<= send_code;
					elsif(store_data(33 downto 32) = "11") then --sending register (command) frame packet
						FM_data				<= x"CF000000";
						FM_data_type		<= "01";
						state				<= send_reg;
					end if;
					
				when send_data =>
					data_rd				<= '1';
					FM_data				<= store_data(31 downto 0);
					FM_data_type		<= "00";
					FM_valid			<= '1';
					store_data_valid	<= data_valid;
					
					if(fifo_empty = '1') then
						FM_data_type		<= "11";
					else
						store_data			<= data_o;
					end if;
					
					if((data_o(33 downto 32) = "00") or (data_o(33 downto 32) = "10") or (data_o(33 downto 32) = "11")) and (data_valid = '1') then  --close data packet
						FM_data_type		<= "10";
						data_rd				<= '0';
						state				<= send_idle;
					end if;
			
					if (empty_cnt = empty_timeout) then
						data_rd				<= '0';
						state				<= close_packet;	
					end if;
			
				when send_reg =>
					data_rd				<= '1';
					FM_data				<= store_data(31 downto 0);
					FM_data_type		<= "00";
					FM_valid			<= '1';
					store_data_valid	<= data_valid;
					if(fifo_empty = '1') then
						FM_data_type		<= "11";
					else
						store_data			<= data_o;
					end if;
					if((data_o(33 downto 32) = "00") or (data_o(33 downto 32) = "01") or (data_o(33 downto 32) = "10")) and (data_valid = '1') then --close dreg packet
						FM_data_type		<= "10";
						data_rd				<= '0';
						state				<= send_idle;
					end if;
			
					if (empty_cnt = empty_timeout) then
						data_rd				<= '0';
						state				<= close_packet;	
					end if;
				
				when send_code =>
					data_rd				<= '1';
					FM_data				<= store_data(31 downto 0);
					FM_data_type		<= "00";
					FM_valid			<= '1';
					store_data_valid	<= data_valid;
					if(fifo_empty = '1') then
						FM_data_type		<= "11";
					else
						store_data			<= data_o;
					end if;
					
					if((data_o(33 downto 32) = "00") or (data_o(33 downto 32) = "01") or (data_o(33 downto 32) = "11")) and (data_valid = '1') then --close aurora code packet
						FM_data_type		<= "10";
						data_rd				<= '0';
						state				<= send_idle;
					end if;
			
					if (empty_cnt = empty_timeout) then
						data_rd				<= '0';
						state				<= close_packet;	
					end if; 
					
				when close_packet =>
					FM_data				<= store_data(31 downto 0);
					FM_data_type		<= "10";
					FM_valid			<= '1';
					if(store_data_valid = '1') then
						state			<= read_en;
					else
						state			<= idle;
					end if;
			end case;
		end if;
	end process FM_data_controller;
	
	

end Behavioral;
