



library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
--use work.I2C.all;
use work.FMTransceiverPackage.all;
use work.vendor_specific_gbt_bank_package.all;

entity GBT_FullMode is
generic(
	GTX_TRANSCEIVERS        : boolean := true; -- true for GTX, false for GTH
	NUM_LINKS               : integer := 1
);
port (
	rst						: in	std_logic;
	--     CLOCK SOURCES
	sysclk					: in	std_logic;
	sma_mgt_refclk			: in	std_logic;
	pll_mgt_refclk			: in	std_logic;
	
	sysclk40				: in	std_logic;
	clk40					: in	std_logic;
	clk120					: in	std_logic;
	clk240					: in	std_logic;
	
	recclk					: out	std_logic; --240 MhZ recovered clk from GBT
	
	--     INPUT AND OUTPUT DATA
	-- tx
	FM_data					: in	slv32_array(NUM_LINKS-1 downto 0);
	FM_data_type			: in	slv2_array(NUM_LINKS-1 downto 0);
	FM_data_valid			: in	std_logic_vector(NUM_LINKS-1 downto 0);
	FM_fifo_full			: out	std_logic_vector(NUM_LINKS-1 downto 0);
	--rx
	GBT_rx_data				: out	slv84_array(NUM_LINKS-1 downto 0);
	GBT_locked				: out	std_logic_vector(NUM_LINKS-1 downto 0);
	
	gtrxn_in				: in	std_logic_vector(NUM_LINKS-1 downto 0);
	gtrxp_in				: in	std_logic_vector(NUM_LINKS-1 downto 0);
	gttxn_out				: out	std_logic_vector(NUM_LINKS-1 downto 0);
	gttxp_out				: out	std_logic_vector(NUM_LINKS-1 downto 0);
	
	--      CONTROL SIGNALS
	cpll_locked				: out	std_logic_vector(NUM_LINKS-1 downto 0);
	qpll_locked				: out	std_logic
);
end entity GBT_FullMode;


architecture structure of GBT_FullMode is
----------------------------------------------------------
--                                                      --
--                  Signal Declaration                  --
--                                                      --
----------------------------------------------------------
	signal FM_data_i									: slv32_array(NUM_LINKS-1 downto 0);
	signal FM_data_type_i								: slv2_array(NUM_LINKS-1 downto 0);
	signal FM_fifo_data_out								: slv34_array(NUM_LINKS-1 downto 0);
	signal FM_fifo_re, FM_FIFO_empty					: std_logic_vector(NUM_LINKS-1 downto 0);
	signal FM_fifo_cnt									: std_logic_vector(7 downto 0);
	
	signal tx_din										: slv32_array(NUM_LINKS-1 downto 0);
	signal tx_kin										: slv4_array(NUM_LINKS-1 downto 0);

	signal tx_usrclk, rx_usrclk							: std_logic;
	signal txprbs_sel									: std_logic_vector(2 downto 0):= "000";
	
	signal cpll_locked_i								: std_logic_vector(NUM_LINKS-1 downto 0);
	
	-- RX signals
	signal rx_data_20									: slv20_array(NUM_LINKS-1 downto 0);
	signal rx_data_i, rx_data_alig						: slv40_array(NUM_LINKS-1 downto 0);
	signal gbt_rx_data_i								: slv84_array(NUM_LINKS-1 downto 0);
	signal rx_head_locked								: std_logic_vector(NUM_LINKS-1 downto 0);
	type bitslip_type									is array (natural range <>) of std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
	signal bitslip_i									: bitslip_type(NUM_LINKS-1 downto 0);
	
	signal tx_fsm_reset_done, rx_fsm_reset_done			: std_logic_vector(NUM_LINKS-1 downto 0);
	signal tx_reset_done, rx_reset_done					: std_logic_vector(NUM_LINKS-1 downto 0);
----------------------------------------------------------
--                                                      --
--                   Module Declaration                 --
--                                                      --
----------------------------------------------------------
	COMPONENT FullMode_Fifo
	PORT (
		rst				: IN STD_LOGIC;
		wr_clk			: IN STD_LOGIC;
		rd_clk			: IN STD_LOGIC;
		din				: IN STD_LOGIC_VECTOR(33 DOWNTO 0);
		wr_en			: IN STD_LOGIC;
		rd_en			: IN STD_LOGIC;
		dout			: OUT STD_LOGIC_VECTOR(33 DOWNTO 0);
		full			: OUT STD_LOGIC;
		empty			: OUT STD_LOGIC;
		rd_data_count	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		prog_full		: OUT STD_LOGIC
	);
	END COMPONENT;
	
	-- GBT RX FIFO CONVERTER
	COMPONENT fifo_20to40
	PORT (
		rst : IN STD_LOGIC;
		wr_clk : IN STD_LOGIC;
		rd_clk : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(39 DOWNTO 0);
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC
	);
	END COMPONENT;
	
	
	-- DEBUGGING
	COMPONENT ila_0
	
	PORT (
		clk : IN STD_LOGIC;	
		probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		probe1 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
	);
	END COMPONENT  ;
	
		-- DEBUGGING
	COMPONENT ila_rx
	
	PORT (
		clk : IN STD_LOGIC;	
		probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		probe1 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
		probe2 : IN STD_LOGIC_VECTOR(83 DOWNTO 0)
	);
	END COMPONENT  ;
	
		
begin

	recclk			<= rx_usrclk;
	cpll_locked		<= cpll_locked_i;
	GBT_rx_data		<= GBT_rx_data_i;
	GBT_locked		<= rx_head_locked;
----------------------------------------------------------
--                                                      --
--                   Module Declaration                 --
--                                                      --
----------------------------------------------------------

	-- debugging
	tx_ila : ila_0	
	PORT MAP (
		clk 				=> tx_usrclk,	
		probe0				=> tx_din(0),
		probe1				=> tx_kin(0),
		probe2				=> FM_fifo_cnt,
		probe3(0)			=> FM_fifo_re(0)
	);
	
	rx_ila : ila_rx	
	PORT MAP (
		clk 				=> clk40,	
		probe0(0)			=> rx_head_locked(0),
		probe1				=> bitslip_i(0),
		probe2				=> gbt_rx_data_i(0)
	);

	FM_data_gen : for i in 0 to (NUM_LINKS-1) generate
	
		FM_data_type_i(i)		<= FM_fifo_data_out(i)(33 downto 32);
		FM_data_i(i)			<= FM_fifo_data_out(i)(31 downto 0);
		
		--      FullMode Data Fifo
		FMData_Fifo: FullMode_Fifo
		Port map (
			rst 				=> rst,
			wr_clk				=> clk240,
			rd_clk				=> tx_usrclk,
			din(33 downto 32)	=> FM_data_type(i),
			din(31 downto 0)	=> FM_data(i),	
			wr_en				=> FM_data_valid(i),
			rd_en				=> FM_fifo_re(i),
			dout				=> FM_fifo_data_out(i),
			full				=> open,
			empty				=> FM_FIFO_empty(i),
			rd_data_count		=> FM_fifo_cnt,
			prog_full			=> FM_fifo_full(i)
		);
		
		--      FullMode Tx Controller
		FullModeTxControl: entity work.FMchannelTXctrl
		port map(
			clk240     => tx_usrclk,
			rst        => rst,
			busy       => '0',
			fifo_rclk  => open,
			fifo_re    => FM_fifo_re(i),
			fifo_dout  => FM_data_i(i),
			fifo_dtype => FM_data_type_i(i),
			fifo_empty => FM_fifo_empty(i),
			dout       => tx_din(i),
			kout       => tx_kin(i)
		);	
		
		--GBT RX
		GBT_data_rx: entity work.gbt_rx
		Generic Map(
			NUM_LINKS						=> NUM_LINKS,
			TX_OPTIMIZATION					=> STANDARD,
			RX_OPTIMIZATION					=> LATENCY_OPTIMIZED,
			TX_ENCODING						=> GBT_FRAME,
			RX_ENCODING						=> GBT_FRAME
		)
		Port Map(
			RX_RESET_I						=> rst,
			-- Clocks:
			RX_WORDCLK_I					=> rx_usrclk,
			RX_FRAMECLK_I					=> clk40,
			    
			-- Control --                            
			
			RX_MGT_RDY_I					=> '1',
			RX_WORDCLK_READY_I				=> '1',  
			RX_FRAMECLK_READY_I				=> '1',   
			RX_BITSLIP_NBR_O				=> bitslip_i(i),
			RX_HEADER_LOCKED_O				=> rx_head_locked(i),
			RX_HEADER_FLAG_O				=> open,
			RX_ISDATA_FLAG_O				=> open,
			RX_READY_O						=> open,
			
			-- Status  --
			RX_ERROR_DETECTED				=> open,
			RX_BIT_MODIFIED_CNTER			=> open,
			         			-- Word & Data --
			RX_WORD_I						=> rx_data_i(i),
			ALIGNED_RX_WORD_O				=> rx_data_alig(i),
			RX_DATA_O						=> gbt_rx_data_i(i),
			
			RX_EXTRA_DATA_WIDEBUS_O			=> open
		
		);  
		
	end generate;
    


	gtx_transceiver:entity work.FM_GBT_transceiver_wrapper
	Generic MAP (
		NUM_LINKS				=> NUM_LINKS
	)
	Port Map( 
		-- clocks
		SYSCLK_IN				=> sysclk40,
		sma_mgt_refclk			=> sma_mgt_refclk,
		pll_mgt_refclk			=> pll_mgt_refclk,
		rxusrclk				=> rx_usrclk,
		txusrclk				=> tx_usrclk,
		
		-- reset ports
		cpllreset_in			=> rst,
		SOFT_RESET_TX_IN		=> rst,
		SOFT_RESET_RX_IN		=> rst,
		
		-- SFP ports
		gtrxp_in				=> gtrxp_in,
		gtrxn_in				=> gtrxn_in,
		gttxn_out				=> gttxn_out,
		gttxp_out				=> gttxp_out,
	
		-- data
		rxdata_out				=> rx_data_i,
		txdata_in				=> tx_din,
		txcharisk_in			=> tx_kin,
		
		--control ports
		rxslide_in				=> (others => '0'),
		TX_FSM_RESET_DONE_OUT	=> tx_fsm_reset_done,
		RX_FSM_RESET_DONE_OUT	=> rx_fsm_reset_done,
		cplllock_out			=> cpll_locked_i,
		qplllock_out			=> qpll_locked,
		rxresetdone_out			=> rx_reset_done,
		txresetdone_out			=> tx_reset_done
	);
	
end architecture structure ; -- of FM_UserExample
