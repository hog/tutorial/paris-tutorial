--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               University of Bologna                                                         
-- Developer:             Nico Giangiacomi (nico.giangiacomi@cern.ch) (nico.giangiacomi@bo.infn.it)    
--                                                                                            
-- Project Name:          Protocol Converter                                                                
-- Module Name:           TTC Generator                                     
--                                                                                            
-- Language:              VHDL'93                                                                  
--                                                                                              
-- Target Device:         Xilinx Kintex 7 & Virtex 7                                                      
-- Tool version:          Vivado 2017.2                                                                
--                                                                                              
-- Version:               1.5                                                                      
--
-- Description:           This module generates TTC-like signals (triggers, ECRs and BCRs).
--					      to be activated if read TTC is not available
--
-- Versions history:      DATE         VERSION   AUTHOR              DESCRIPTION
--
--                        29/01/2018   1.0       Nico Giangiacomi    First .vhd module definition       
--
-- Additional Comments:                                                                          
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TTC_generator is
Generic (
	trig_interval	: integer:= 400;	--100 mHz trigger
	BCR_interval	: integer:= 4001;  -- BCR every 1ms
	ECR_interval	: integer:= 200000001 --ECR every 5 s 
);
Port ( 
	clk40			: in std_logic;
	rst				: in std_logic;
	trig_out		: out std_logic;
	ECR_out			: out std_logic;
	BCR_out			: out std_logic
);
end TTC_generator;

architecture Behavioral of TTC_generator is
--########################################################################
--##																	##
--##						Signals declaration							##
--##																	##
--########################################################################
	signal trig_cnt, BCR_cnt, ECR_cnt		: integer:=0;
begin

--########################################################################
--##																	##
--##						Processes implementation					##
--##																	##
--########################################################################

	TTC_generation: process(rst, clk40)
	begin	
		if(rst = '1') then
			trig_cnt		<= 0;
			BCR_cnt			<= 0;
			ECR_cnt			<= 0;
			trig_out		<= '0';
			ECR_out			<= '0';
			BCR_out			<= '0';
		elsif(rising_edge(clk40)) then
			trig_out		<= '0';
			ECR_out			<= '0';
			BCR_out			<= '0';
			trig_cnt		<= trig_cnt + 1;
			BCR_cnt			<= BCR_cnt + 1;
			ECR_cnt			<= ECR_cnt + 1;
			if(trig_cnt = trig_interval) then
				trig_cnt		<= 0;
				trig_out		<= '1';
			end if;
			if(ECR_cnt = ECR_interval) then
				ECR_cnt			<= 0;
				ECR_out			<= '1';
			end if;
			if(BCR_cnt = BCR_interval) then
				BCR_cnt			<= 0;
				BCR_out			<= '1';
			end if;
		end if;	
	end process TTC_generation;

end Behavioral;
