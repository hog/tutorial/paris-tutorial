--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               University of Bologna                                                         
-- Developer:             Nico Giangiacomi (nico.giangiacomi@cern.ch) (nico.giangiacomi@bo.infn.it)    
--                                                                                            
-- Project Name:          Protocol Converter                                                                
-- Module Name:           RD53A TTC Encoder                                       
--                                                                                            
-- Language:              VHDL'93                                                                  
--                                                                                              
-- Target Device:         Xilinx Kintex 7 & Virtex 7                                                      
-- Tool version:          Vivado 2017.2                                                                
--                                                                                              
-- Version:               1.5                                                                      
--
-- Description:        
--
-- Versions history:      DATE         VERSION   AUTHOR              DESCRIPTION
--
--                        29/01/2018   1.0       Nico Giangiacomi    First .vhd module definition  
--						  15/03/2018   1.5       Nico Giangiacomi    Added Command Ready process         
--
-- Additional Comments:                                                                          
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity RD53_TTC_Encoder is       
port(  
 
	clk_40		: in std_logic;
	clk_80		: in std_logic;
	clk_160		: in std_logic;
	clk_320		: in std_logic;
	
	rst			: in std_logic;
	
	ttc_elink	: in std_logic_vector(3 downto 0);
	data_elink	: in std_logic_vector(3 downto 0);
	
	TTC_gen_en	: in std_logic;
	ttc_out		: out std_logic
);
end RD53_TTC_Encoder;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of RD53_TTC_Encoder is

----------------------------------------------------------
--                                                      --
--                  Signals Declaration                 --
--                                                      --
----------------------------------------------------------        
	
	signal trig_sr, ecr_sr, bcr_sr			: std_logic_vector(3 downto 0):= (others => '0');
	signal data2ttc							: std_logic_vector(15 downto 0):= (others => '0');
	signal efifo_dout						: std_logic_vector(15 downto 0);
	signal efifo_out_valid					: std_logic:= '0';
	
	signal cmd_data_in, cmd_data_out		: std_logic_vector(15 downto 0);
	signal cmd_data_we, cmd_data_re			: std_logic:= '0';
	signal fifo_occ							: std_logic_vector(9 downto 0);
	signal data2ttc_fifo					: std_logic_vector(3 downto 0):= (others => '0');
	signal cmd_data_empy					: std_logic:=	'1';
	signal word_cnt							: integer range 0 to 3:= 0;
	
	type state_type is (reset, wait_for_cmd, process_cmd);
	signal cmd_in_state						: state_type := reset;
	signal cmd_out_state					: state_type:= reset;
	signal cmd_ready						: std_logic := '0';
	signal word_left_cnt					: integer range 0 to 11:=0;
	signal cmd_left_cnt						: integer range 0 to 12:=0;
	
	-- TTC generator signals
	signal rst_TTC_gen						: std_logic;
	signal trig_gen, ECR_gen, BCR_gen		: std_logic;
	
	
	signal L1ID_cnt							: integer range 0 to 31:= 0;
	signal trig_tag							: std_logic_vector(7 downto 0);
	signal trig_arrived						: std_logic;
	
----------------------------------------------------------
--                                                      --
--                 Module Declaration                   --
--                                                      --
----------------------------------------------------------        
	COMPONENT fifo_40to160
	PORT (
		rst : IN STD_LOGIC;
		wr_clk : IN STD_LOGIC;
		rd_clk : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC
	);
	END COMPONENT;
	
	COMPONENT cmd_fifo
	PORT (
		clk : IN STD_LOGIC;
		srst : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC;
		data_count : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
	END COMPONENT;
	
	COMPONENT ttc_ila
	PORT (
		clk : IN STD_LOGIC;
		probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
		probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
		probe2 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
		probe3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
		probe4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		probe5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
	END COMPONENT  ;
	--=====================================================================================-- 
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 

----------------------------------------------------------
--                                                      --
--                 Module Implementation                --
--                                                      --
----------------------------------------------------------
	TTC_fifo: fifo_40to160
	Port Map (
		rst 		=> rst,
		wr_clk 		=> clk_40,
		rd_clk 		=> clk_160,
		din 		=> data2ttc_fifo,
		wr_en 		=> '1',
		rd_en 		=> '1',
		dout(0)		=> ttc_out,
		full 		=> open,
		empty 		=> open
	);	
	
	cmd_fifo_inst: cmd_fifo
	Port Map (
		clk 		=> clk_40,
		srst		=> rst,
		din			=> cmd_data_in,
		wr_en		=> cmd_data_we,
		rd_en		=> cmd_data_re,
		dout		=> cmd_data_out,
		full		=> open,
		empty		=> cmd_data_empy,
		data_count	=> fifo_occ
	);
	
	--elink2fifo ---- elink receiver 
	elink_rx: entity work.Elink2FIFO
	generic map (
		InputDataRate       => 160,
		elinkEncoding       => "01",
		serialized_input    => false
	)
	port map ( 
		clk40       => clk_40,
		clk80       => clk_80,
		clk160      => clk_160,    
		clk320      => clk_320,
		rst         => rst,
		fifo_flush  => '0',
		------
		DATA1bitIN  => '0',
		elink2bit   => "00",
		elink4bit   => data_elink,
		elink8bit   => x"00",
		elink16bit  => x"0000",
		------
		efifoRclk   => clk_40,
		efifoRe     => '1', 
		efifoHF     => efifo_out_valid,
		efifoDout   => efifo_dout
	------
	);
	
	ttc_debug : ttc_ila
	PORT MAP (
		clk 		=> clk_40,
		probe0(0) 	=> efifo_out_valid, 
		probe1 		=> efifo_dout, 
		probe2 		=> trig_sr, 
		probe3 		=> ecr_sr, 
		probe4 		=> bcr_sr,
		probe5 		=> data2ttc
	);
	
	rst_TTC_gen		<= rst or (not TTC_gen_en);
	ttc_generator_impl: entity work.TTC_generator
	Generic Map(
		trig_interval	=> 4000,		-- 10 mHz trigger
		BCR_interval	=> 4010,  		-- BCR every 1ms
		ECR_interval	=> 200000101 	-- ECR every 5s 
	)
	Port Map( 
		clk40			=> clk_40,
		rst				=> rst_TTC_gen,
		trig_out		=> trig_gen,
		ECR_out			=> ECR_gen,
		BCR_out			=> BCR_gen
	);
	
----------------------------------------------------------
--                                                      --
--                Process Implementation                --
--                                                      --
----------------------------------------------------------
	shift_register_process : process(rst, clk_40)
	begin
		if(rst = '1') then
			trig_sr		<= (others => '0');
			ecr_sr		<= (others => '0');
			bcr_sr		<= (others => '0');
		elsif(rising_edge(clk_40)) then
			if(TTC_gen_en = '0') then
				trig_sr(0)		<= ttc_elink(0);
				bcr_sr(0)		<= ttc_elink(1);
				ecr_sr(0)		<= ttc_elink(2);
			else
				trig_sr(0)		<= trig_gen;
				bcr_sr(0)		<= BCR_gen;
				ecr_sr(0)		<= ECR_gen;
			end if;
			for i in 3 downto 1 loop
				trig_sr(i)		<= trig_sr(i-1);
				bcr_sr(i)		<= bcr_sr(i-1);
				ecr_sr(i)		<= ecr_sr(i-1);
			end loop;
		end if;
	end process shift_register_process;

	trig_arrived		<= ttc_elink(0) or trig_gen;
	l1ID_counter_process: process(rst, clk_40)
	begin
		if (rst = '1') then
			L1ID_cnt		<= 0;
		elsif(rising_edge(clk_40)) then
			if(ecr_sr /= x"0") then
				L1ID_cnt		<= 0;
			elsif(trig_arrived = '1') then
				L1ID_cnt		<= L1ID_cnt + 1;
			
				case L1ID_cnt is
					when 31 =>
						trig_tag	<= x"6A";
					when 0 =>
						trig_tag	<= x"6C";
					when 1 =>
						trig_tag	<= x"71";
					when 2 =>
						trig_tag	<= x"72";
					when 3 =>
						trig_tag	<= x"74";
					when 4 =>
						trig_tag	<= x"8B";
					when 5 =>
						trig_tag	<= x"8D";
					when 6 =>
						trig_tag	<= x"8E";
					when 7 =>
						trig_tag	<= x"93";
					when 8 =>
						trig_tag	<= x"95";
					when 9 =>
						trig_tag	<= x"96";
					when 10 =>
						trig_tag	<= x"99";
					when 11 =>
						trig_tag	<= x"9A";
					when 12 =>
						trig_tag	<= x"9C";
					when 13 =>
						trig_tag	<= x"A3";
					when 14 =>
						trig_tag	<= x"A5";						
					when 15 =>
						trig_tag	<= x"A6";
					when 16 =>
						trig_tag	<= x"A9";
					when 17 =>
						trig_tag	<= x"AA";
					when 18 =>
						trig_tag	<= x"AC";
					when 19 =>
						trig_tag	<= x"B1";
					when 20 =>
						trig_tag	<= x"B2";
					when 21 =>
						trig_tag	<= x"B4";
					when 22 =>
						trig_tag	<= x"C3";
					when 23 =>
						trig_tag	<= x"C5";
					when 24 =>
						trig_tag	<= x"C6";
					when 25 =>
						trig_tag	<= x"C9";
					when 26 =>
						trig_tag	<= x"CA";
					when 27 =>
						trig_tag	<= x"CC";
					when 28 =>
						trig_tag	<= x"D1";
					when 29 =>
						trig_tag	<= x"D2";
					when 30 =>
						trig_tag	<= x"D4";
				end case;
			end if;
		end if;
	end process l1ID_counter_process;


	cmd_sel_process: process(clk_40)
	begin
		if (rst = '1') then
			cmd_in_state		<= reset;
			cmd_data_we			<= '0';
		elsif(rising_edge(clk_40)) then
			cmd_data_we			<= '0';
			case cmd_in_state is
				when reset =>
					cmd_in_state		<= wait_for_cmd;
				when wait_for_cmd =>
					if (efifo_out_valid = '1') then
						case efifo_dout is
							when x"5C5C" =>		-- global pulse
								word_left_cnt		<= 1; 
								cmd_data_we			<= '1';
								cmd_data_in			<= efifo_dout;
								cmd_in_state		<= process_cmd;
							when x"6363" =>		-- cal
								word_left_cnt		<= 2; 
								cmd_data_we			<= '1';
								cmd_data_in			<= efifo_dout;
								cmd_in_state		<= process_cmd;
							when x"6666" =>		-- writeReg (short)
								word_left_cnt		<= 3; 
								cmd_data_we			<= '1';
								cmd_data_in			<= efifo_dout;
								cmd_in_state		<= process_cmd;		 
							when x"6667" =>		-- writeReg (long)
								word_left_cnt		<= 11; 
								cmd_data_we			<= '1';
								cmd_data_in			<= efifo_dout;
								cmd_in_state		<= process_cmd;
							when x"6565" =>		-- ReadReg 
								word_left_cnt		<= 2; 
								cmd_data_we			<= '1';
								cmd_data_in			<= efifo_dout;
								cmd_in_state		<= process_cmd;
							when x"6969" =>		-- Noop
								cmd_data_we			<= '1';
								cmd_data_in			<= efifo_dout;
							when others =>
								cmd_data_we			<= '0';
						end case;
					end if;
				when process_cmd =>
					if (efifo_out_valid = '1') then
						cmd_data_we			<= '1';
						word_left_cnt		<= word_left_cnt - 1;
						if (word_left_cnt = 1) then
							cmd_in_state <= wait_for_cmd;
						end if;
						for i in 0 to 1 loop
							case efifo_dout(4 + 5*i downto 5*i) is
								when "00000" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"6A";
								when "00001" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"6C";
								when "00010" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"71";
								when "00011" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"72";
								when "00100" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"74";
								when "00101" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"8B";
								when "00110" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"8D";
								when "00111" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"8E";
								when "01000" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"93";
								when "01001" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"95";
								when "01010" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"96";
								when "01011" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"99";
								when "01100" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"9A";
								when "01101" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"9C";
								when "01110" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"A3";
								when "01111" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"A5";						
								when "10000" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"A6";
								when "10001" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"A9";
								when "10010" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"AA";
								when "10011" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"AC";
								when "10100" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"B1";
								when "10101" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"B2";
								when "10110" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"B4";
								when "10111" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"C3";
								when "11000" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"C5";
								when "11001" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"C6";
								when "11010" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"C9";
								when "11011" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"CA";
								when "11100" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"CC";
								when "11101" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"D1";
								when "11110" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"D2";
								when "11111" =>
									cmd_data_in(7+8*i downto 8*i)	<= x"D4";
							end case;
						end loop;
					end if;
			end case;
		end if;
	end process cmd_sel_process;

	cmd_ready_process: process(rst, clk_40)
	begin
		if(rst = '1') then
				cmd_out_state	<= reset;
				cmd_ready	<= '0';
		elsif(rising_edge(clk_40)) then
			case cmd_out_state is
				when reset =>
					cmd_out_state		<= wait_for_cmd;
					cmd_left_cnt		<= 0;
				when wait_for_cmd =>
					cmd_ready		<= '0';
					if(cmd_data_out = x"5C5C") and (unsigned(fifo_occ) > 1) then		-- global pulse
						cmd_ready 		<= '1';
						cmd_left_cnt	<= 2;
						cmd_out_state	<= process_cmd;
					elsif(cmd_data_out = x"6363") and (unsigned(fifo_occ) > 2) then		-- cal
						cmd_ready 		<= '1';
						cmd_out_state	<= process_cmd;
						cmd_left_cnt	<= 3;
					elsif(cmd_data_out = x"6666") and (unsigned(fifo_occ) > 3) then		-- writeReg (short)
						cmd_ready 		<= '1';
						cmd_out_state	<= process_cmd;
						cmd_left_cnt	<= 4;
					elsif(cmd_data_out = x"6667") and (unsigned(fifo_occ) > 11) then	-- writeReg (long)
						cmd_ready 		<= '1';
						cmd_out_state	<= process_cmd;
						cmd_left_cnt	<= 12;
					elsif(cmd_data_out = x"6565") and (unsigned(fifo_occ) > 2) then		-- readReg
						cmd_ready 		<= '1';
						cmd_out_state	<= process_cmd;
						cmd_left_cnt	<= 3;
					elsif(cmd_data_out = x"6969") then									-- noop
						cmd_ready 		<= '1';
						cmd_out_state	<= process_cmd;
						cmd_left_cnt	<= 1;
					end if;
				when process_cmd =>
					cmd_ready			<= '1';
					if(cmd_data_re = '1') then
						cmd_left_cnt <= cmd_left_cnt - 1;
					end if;
					if (cmd_left_cnt = 0) then
						cmd_ready			<= '0';
						cmd_out_state		<= wait_for_cmd;
					end if;
			end case;
		end if;	
	end process cmd_ready_process;

	--swap bits in data2ttc nibbles
	data2ttc_fifo(0)	<= data2ttc(4*(3-word_cnt));
	data2ttc_fifo(1)	<= data2ttc(4*(3-word_cnt) +1);
	data2ttc_fifo(2)	<= data2ttc(4*(3-word_cnt) +2);
	data2ttc_fifo(3)	<= data2ttc(4*(3-word_cnt) +3);
	output_select_process : process(rst, clk_40)
	begin
		if(rst = '1') then
			word_cnt		<= 0;
			cmd_data_re		<= '0';
		elsif(rising_edge(clk_40)) then
			cmd_data_re		<= '0';
			if(word_cnt = 3) then
				if(ecr_sr /= x"0") then
					word_cnt	<= 0;
					data2ttc	<= x"5A5A";
				elsif(bcr_sr /= x"0") then
					word_cnt	<= 0;
					data2ttc	<= x"5959";
				elsif(trig_sr /= x"0") then
					word_cnt	<= 0;
					case trig_sr is
						when x"1" =>
							data2ttc	<= x"2B" & trig_tag;
						when x"2" =>
							data2ttc	<= x"2D" & trig_tag;
						when x"3" =>
							data2ttc	<= x"2E" & trig_tag;
						when x"4" =>
							data2ttc	<= x"33" & trig_tag;
						when x"5" =>
							data2ttc	<= x"35" & trig_tag;
						when x"6" =>
							data2ttc	<= x"36" & trig_tag;
						when x"7" =>
							data2ttc	<= x"39" & trig_tag;
						when x"8" =>
							data2ttc	<= x"3A" & trig_tag;
						when x"9" =>
							data2ttc	<= x"3C" & trig_tag;
						when x"A" =>
							data2ttc	<= x"4B" & trig_tag;
						when x"B" =>
							data2ttc	<= x"4D" & trig_tag;
						when x"C" =>
							data2ttc	<= x"4E" & trig_tag;
						when x"D" =>
							data2ttc	<= x"53" & trig_tag;
						when x"E" =>
							data2ttc	<= x"55" & trig_tag;
						when x"F" =>
							data2ttc	<= x"56" & trig_tag;
						when others =>
							data2ttc	<= x"0000";
					end case;
				elsif(cmd_ready = '1') then
					cmd_data_re	<= '1';
					word_cnt	<= 0;
					data2ttc	<= cmd_data_out;
				else 
					word_cnt	<= 0;
					data2ttc	<= x"817E";
				end if;
			else
				word_cnt	<= word_cnt + 1;
			end if;
			
		end if;
	end process output_select_process;

   --=====================================================================================--   
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--