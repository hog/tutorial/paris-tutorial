----------------------------------------------------------------------------------
-- Company: INFN & University of Bologna
-- Developer: Nico Giangiacomi
--
-- Create Date: 02/13/2018 03:58:37 PM
-- Design Name: PiLUP_Protocol_Converter
-- Module Name: PiLUP_Protocol_Converter_top - Behavioral
-- Project Name: PiLUP_Protocol_Converter
-- Target Devices: Bologna INFN PiLUP Board
-- Tool Versions: 1.0
-- Description: Protocol converter to interface FE chip RD53A to readout board Felix
--
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.register_types.all;
use work.FMTransceiverPackage.all;

entity PiLUP_Protocol_Converter_top is
	Port (

	--  CLOCK SIGNALS
		SYSCLK_P 		: in std_logic;
		SYSCLK_N 		: in std_logic;
		SMA_MGT_REFCLK_N		: in std_logic;
		SMA_MGT_REFCLK_P		: in std_logic;
		SI5326_OUT_C_N		: in std_logic;
		SI5326_OUT_C_P		: in std_logic;
		REC_CLOCK_C_N		: out std_logic;
		REC_CLOCK_C_P		: out std_logic;
		USER_CLOCK_P		: in std_logic;
		USER_CLOCK_N		: in std_logic;
		USER_SMA_CLOCK_P		: out std_logic;
		USER_SMA_CLOCK_N		: out std_logic;

	--  SFP SIGNALS
		SFP_RX_N		: in std_logic;
		SFP_RX_P		: in std_logic;
		SFP_TX_N		: out std_logic;
		SFP_TX_P		: out std_logic;
		SFP_TX_DISABLE		: out std_logic;

	-- IP BLOB external interfaces (C2C + i2c)
		AXI_C2C_IN_N : in std_logic_vector ( 8 downto 0 );
		AXI_C2C_IN_P : in std_logic_vector ( 8 downto 0 );
		AXI_C2C_OUT_N : out std_logic_vector ( 8 downto 0 );
		AXI_C2C_OUT_P : out std_logic_vector ( 8 downto 0 );
		KZ_BUS_CLK_N : in std_logic;
		KZ_BUS_CLK_P : in std_logic;
		KZ_CLK_OUT_N : out std_logic;
		KZ_CLK_OUT_P : out std_logic;
		ext_reset_in : in std_logic;
		iic_main_scl_io : inout std_logic;
		iic_main_sda_io : inout std_logic
	);
end PiLUP_Protocol_Converter_top;

architecture Behavioral of PiLUP_Protocol_Converter_top is
	----------------------------------------------------------
	--                                                      --
	--                  Signal Declaration                  --
	--                                                      --
	----------------------------------------------------------

	--clock signals
	signal sysclk, sysclk40        	: std_logic;
	signal sma_mgt_refclk						: std_logic;
	signal pll_mgt_refclk						: std_logic;
	signal recclk										: std_logic;
	signal clk40, clk80, clk120, clk160, clk240	: std_logic;
	signal reset_i									: std_logic:= '0';

	-- FullMode tx signals
	signal FM_data_type							: std_logic_vector(1 downto 0):= "11";
	signal FM_data								: std_logic_vector(31 downto 0):= (others => '0');
	signal FM_data_valid, FM_fifo_full_i		: std_logic;
	type gen_data is (boot, sop, eop, data);
	signal FM_state								: gen_data := boot;

	--GBT rx signals
	signal GBT_rx_data							: std_logic_vector(83 downto 0);
	signal GBT_locked_i							: std_logic;

	--RD53A Emulator signals
	signal rst_emulator     : std_logic;
	signal ttc_i            : std_logic;
	signal RD53_data        : slv64_array(3 downto 0);
	signal RD53_header      : std_logic_vector(1 downto 0);
	signal clk160_RD53      : std_logic;

	signal status : reg_matrix(0 to N_STATUS_REGS-1):= (others => (others => '0'));
	signal ctrl : reg_matrix(0 to N_CTRL_REGS-1);

	-- Axi interface signals
	signal AXI_reg_matrix_araddr   : std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
	signal AXI_reg_matrix_arprot   : std_logic_vector (2 downto 0);
	signal AXI_reg_matrix_arready  : std_logic;
	signal AXI_reg_matrix_arvalid  : std_logic;
	signal AXI_reg_matrix_awaddr   : std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
	signal AXI_reg_matrix_awprot   : std_logic_vector (2 downto 0);
	signal AXI_reg_matrix_awready  : std_logic;
	signal AXI_reg_matrix_awvalid  : std_logic;
	signal AXI_reg_matrix_bready   : std_logic;
	signal AXI_reg_matrix_bresp    : std_logic_vector (1 downto 0);
	signal AXI_reg_matrix_bvalid   : std_logic;
	signal AXI_reg_matrix_rdata    : std_logic_vector (31 downto 0);
	signal AXI_reg_matrix_rready   : std_logic;
	signal AXI_reg_matrix_rresp    : std_logic_vector (1 downto 0);
	signal AXI_reg_matrix_rvalid   : std_logic;
	signal AXI_reg_matrix_wdata    : std_logic_vector (31 downto 0);
	signal AXI_reg_matrix_wready   : std_logic;
	signal AXI_reg_matrix_wstrb    : std_logic_vector (3 downto 0);
	signal AXI_reg_matrix_wvalid   : std_logic;

	----------------------------------------------------------
	--                                                      --
	--                   Module Declaration                 --
	--                                                      --
	----------------------------------------------------------

	component IP_blob is
		generic(
			C_S_AXI_ADDR_WIDTH : integer := REGS_AXI_ADDR_WIDTH
		);
		port (
			iic_main_scl_i : in std_logic;
			iic_main_scl_o : out std_logic;
			iic_main_scl_t : out std_logic;
			iic_main_sda_i : in std_logic;
			iic_main_sda_o : out std_logic;
			iic_main_sda_t : out std_logic;
			KZ_BUS_CLK_P : in std_logic;
			KZ_BUS_CLK_N : in std_logic;
			KZ_CLK_OUT_P : out std_logic;
			KZ_CLK_OUT_N : out std_logic;
			AXI_C2C_IN_P : in std_logic_vector(8 downto 0);
			AXI_C2C_IN_N : in std_logic_vector(8 downto 0);
			AXI_C2C_OUT_P : out std_logic_vector(8 downto 0);
			AXI_C2C_OUT_N : out std_logic_vector(8 downto 0);
			AXI_CLK200  :  in std_logic;
			AXI_register_matrix_araddr : out std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
			AXI_register_matrix_arprot : out std_logic_vector (2 downto 0);
			AXI_register_matrix_arready : in std_logic;
			AXI_register_matrix_arvalid : out std_logic;
			AXI_register_matrix_awaddr : out std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
			AXI_register_matrix_awprot : out std_logic_vector (2 downto 0);
			AXI_register_matrix_awready : in std_logic;
			AXI_register_matrix_awvalid : out std_logic;
			AXI_register_matrix_bready : out std_logic;
			AXI_register_matrix_bresp : in std_logic_vector (1 downto 0);
			AXI_register_matrix_bvalid : in std_logic;
			AXI_register_matrix_rdata : in std_logic_vector (31 downto 0);
			AXI_register_matrix_rready : out std_logic;
			AXI_register_matrix_rresp : in std_logic_vector (1 downto 0);
			AXI_register_matrix_rvalid : in std_logic;
			AXI_register_matrix_wdata : out std_logic_vector (31 downto 0);
			AXI_register_matrix_wready : in std_logic;
			AXI_register_matrix_wstrb : out std_logic_vector (3 downto 0);
			AXI_register_matrix_wvalid : out std_logic;
			ext_reset_in : in std_logic
		);
	end component IP_blob;

	component IOBUF is
		port (
			I : in std_logic;
			O : out std_logic;
			T : in std_logic;
			IO : inout std_logic
		);
	end component IOBUF;

	signal iic_main_scl_i : std_logic;
	signal iic_main_scl_o : std_logic;
	signal iic_main_scl_t : std_logic;
	signal iic_main_sda_i : std_logic;
	signal iic_main_sda_o : std_logic;
	signal iic_main_sda_t : std_logic;

begin

	----------------------------------------------------------
	--                                                      --
	--                 Module Implementation                --
	--                                                      --
	----------------------------------------------------------

	-- CLOCK CONFIGURATION
	clk_manager : entity work.clock_manager
	Port Map(
		rst					=> reset_i,
		SYSCLK_P			=> SYSCLK_P,
		SYSCLK_N			=> SYSCLK_N,
		SMA_MGT_REFCLK_N	=> SMA_MGT_REFCLK_N,
		SMA_MGT_REFCLK_P	=> SMA_MGT_REFCLK_P,
		SI5326_OUT_C_N		=> SI5326_OUT_C_N,
		SI5326_OUT_C_P		=> SI5326_OUT_C_P,
		recclk				=> recclk,
		USER_CLOCK_P => USER_CLOCK_P,
		USER_CLOCK_N => USER_CLOCK_N,

		--  OUTPUT CLOCKS
		REC_CLOCK_C_N		=> REC_CLOCK_C_N,
		REC_CLOCK_C_P		=> REC_CLOCK_C_P,

		sysclk				=> sysclk,	--200 MHz clk
		sysclk_40			=> sysclk40,  --40 MHz clk from sysclk
		clk40				=> clk40, --from recclk
		clk80				=> clk80, --from recclk
		clk120				=> clk120, --from recclk
		clk160				=> clk160, --from recclk
		clk240				=> clk240, --from recclk
		sma_mgt_refclk		=> sma_mgt_refclk,	--240 MHz clk
		pll_mgt_refclk		=> pll_mgt_refclk,	--240 MHz clk
		rec_clk_pll_locked	=> status(rec_clk_pll_locked_r)(rec_clk_pll_locked_range),
		USER_SMA_CLOCK_P	=> USER_SMA_CLOCK_P,
		USER_SMA_CLOCK_N    => USER_SMA_CLOCK_N
	);


	--   GBT/ FULL MODE configuration
	GBT_FullMode_connection: entity work.GBT_FullMode
	generic map(
		GTX_TRANSCEIVERS		=> true, -- true for GTX, false for GTH
		NUM_LINKS				=> 1
	)
	port map (
		rst						=> reset_i,
		--     CLOCK SOURCES
		sysclk					=> sysclk,
		sma_mgt_refclk			=> sma_mgt_refclk,
		pll_mgt_refclk			=> pll_mgt_refclk,

		sysclk40				=> sysclk40,
		clk40					=> clk40,
		clk120					=> clk120,
		clk240					=> clk240,

		recclk					=> recclk, --120 MhZ recovered clk from GBT

		--     INPUT AND OUTPUT DATA
		FM_data(0)				=> FM_data,
		FM_data_type(0)			=> FM_data_type,
		FM_data_valid(0)		=> FM_data_valid,
		FM_fifo_full(0)			=> FM_fifo_full_i,

		GBT_rx_data(0)			=> GBT_rx_data,
		GBT_locked(0)			=> GBT_locked_i,

		gtrxn_in(0)				=> SFP_RX_N,
		gtrxp_in(0)				=> SFP_RX_P,
		gttxn_out(0)			=> SFP_TX_N,
		gttxp_out(0)			=> SFP_TX_P,

		--     CONTROL SIGNALS
		cpll_locked(0)		=> status(cpll_locked_r)(cpll_locked_range),
		qpll_locked				=> status(qpll_locked_r)(qpll_locked_range)
	);

	rst_emulator		<= reset_i or not GBT_locked_i;
	TTC_Encoder: entity work.RD53_TTC_Encoder
	Port Map(

		clk_40			=> clk40,
		clk_80			=> clk80,
		clk_160			=> clk160,
		clk_320			=> '0',

		rst				=> rst_emulator,

		ttc_elink		=> GBT_rx_data(11 downto 8),
		data_elink		=> GBT_rx_data(7 downto 4),

		TTC_gen_en		=> ctrl(TTC_en_r)(TTC_en_range),

		ttc_out			=> ttc_i
	);
	RD53_emulator: entity work.RD53_Emulator_wrapper
	Port Map (
		rst				=> rst_emulator,
		clk160_in		=> clk160,
		clk160_out		=> clk160_RD53,
		ttc_in			=> ttc_i,
		chip_id			=> x"0",
		n_hits_in		=> ctrl(n_hits_r)(n_hits_range),
		RD53_data		=> RD53_data,
		RD53_header		=> RD53_header
	);


	Aurora_FM_converter: entity work.Aurora_to_FM_wrapper
	Port Map(
		rst				=> rst_emulator,
		clk160			=> clk160_RD53,
		clk240_rd		=> clk240,
		RD53_data		=> RD53_data,
		RD53_header		=> RD53_header,
		FM_data			=> FM_data,
		FM_data_type	=> FM_data_type,
		FM_valid		=> FM_data_valid
	);


	register_block: entity work.axi_regs_test
	generic map(
		C_S_AXI_ADDR_WIDTH => REGS_AXI_ADDR_WIDTH
	)
	port map(
		S_AXI_ACLK	 	=> sysclk,
		S_AXI_ARESETN	=> ext_reset_in,
		S_AXI_AWADDR 	=> AXI_reg_matrix_awaddr,
		S_AXI_AWPROT 	=> AXI_reg_matrix_awprot,
		S_AXI_AWVALID	=> AXI_reg_matrix_awvalid,
		S_AXI_AWREADY	=> AXI_reg_matrix_awready,
		S_AXI_WDATA	 	=> AXI_reg_matrix_wdata,
		S_AXI_WSTRB	 	=> AXI_reg_matrix_wstrb,
		S_AXI_WVALID 	=> AXI_reg_matrix_wvalid,
		S_AXI_WREADY 	=> AXI_reg_matrix_wready,
		S_AXI_BRESP	 	=> AXI_reg_matrix_bresp,
		S_AXI_BVALID 	=> AXI_reg_matrix_bvalid,
		S_AXI_BREADY 	=> AXI_reg_matrix_bready,
		S_AXI_ARADDR 	=> AXI_reg_matrix_araddr,
		S_AXI_ARPROT 	=> AXI_reg_matrix_arprot,
		S_AXI_ARVALID	=> AXI_reg_matrix_arvalid,
		S_AXI_ARREADY	=> AXI_reg_matrix_arready,
		S_AXI_RDATA	 	=> AXI_reg_matrix_rdata,
		S_AXI_RRESP	 	=> AXI_reg_matrix_rresp,
		S_AXI_RVALID 	=> AXI_reg_matrix_rvalid,
		S_AXI_RREADY 	=> AXI_reg_matrix_rready,

		STATUS => status,
		CTRL => ctrl
	);

	IP_block_i: component IP_blob
	port map (
		AXI_C2C_IN_N => AXI_C2C_IN_N,
		AXI_C2C_IN_P => AXI_C2C_IN_P,
		AXI_C2C_OUT_N => AXI_C2C_OUT_N,
		AXI_C2C_OUT_P => AXI_C2C_OUT_P,
		KZ_BUS_CLK_N => KZ_BUS_CLK_N,
		KZ_BUS_CLK_P => KZ_BUS_CLK_P,
		KZ_CLK_OUT_N => KZ_CLK_OUT_N,
		KZ_CLK_OUT_P => KZ_CLK_OUT_P,
		ext_reset_in => ext_reset_in,
		iic_main_scl_i => iic_main_scl_i,
		iic_main_scl_o => iic_main_scl_o,
		iic_main_scl_t => iic_main_scl_t,
		iic_main_sda_i => iic_main_sda_i,
		iic_main_sda_o => iic_main_sda_o,
		iic_main_sda_t => iic_main_sda_t,
		AXI_CLK200 => sysclk,

		AXI_register_matrix_araddr  => AXI_reg_matrix_araddr,
		AXI_register_matrix_arprot  => AXI_reg_matrix_arprot,
		AXI_register_matrix_arready => AXI_reg_matrix_arready,
		AXI_register_matrix_arvalid => AXI_reg_matrix_arvalid,
		AXI_register_matrix_awaddr  => AXI_reg_matrix_awaddr,
		AXI_register_matrix_awprot  => AXI_reg_matrix_awprot,
		AXI_register_matrix_awready => AXI_reg_matrix_awready,
		AXI_register_matrix_awvalid => AXI_reg_matrix_awvalid,
		AXI_register_matrix_bready  => AXI_reg_matrix_bready,
		AXI_register_matrix_bresp   => AXI_reg_matrix_bresp,
		AXI_register_matrix_bvalid  => AXI_reg_matrix_bvalid,
		AXI_register_matrix_rdata   => AXI_reg_matrix_rdata,
		AXI_register_matrix_rready  => AXI_reg_matrix_rready,
		AXI_register_matrix_rresp   => AXI_reg_matrix_rresp,
		AXI_register_matrix_rvalid  => AXI_reg_matrix_rvalid,
		AXI_register_matrix_wdata   => AXI_reg_matrix_wdata,
		AXI_register_matrix_wready  => AXI_reg_matrix_wready,
		AXI_register_matrix_wstrb   => AXI_reg_matrix_wstrb,
		AXI_register_matrix_wvalid  => AXI_reg_matrix_wvalid
	);

	reset_i <= ctrl(reset_r)(reset_range);
	SFP_TX_DISABLE <= ctrl(SFP_TX_DISABLE_r)(SFP_TX_DISABLE_range);

	iic_main_scl_iobuf: component IOBUF
	port map(
		I => iic_main_scl_o,
		IO => iic_main_scl_io,
		O => iic_main_scl_i,
		T => iic_main_scl_t
	);
	iic_main_sda_iobuf: component IOBUF
	port map(
		I => iic_main_sda_o,
		IO => iic_main_sda_io,
		O => iic_main_sda_i,
		T => iic_main_sda_t
	);

end Behavioral;
