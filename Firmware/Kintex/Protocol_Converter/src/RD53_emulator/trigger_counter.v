// controls sending trigger data out of the system
// active flag controls whether it will pass out
// trig_done; trig done asks FIFO for next word

// testing; trigger.do, trigger_tb.v

module trigger_counter (
   input  rst, clk80,
   input  [3:0] datain,
   output trig_out,
   output trig_done
);

reg [3:0] trig_sr;
reg [1:0] shift_cnt;
reg active;

always @ (posedge clk80 or posedge rst) begin
   if (rst) begin
      trig_sr   <= 4'h0;
      shift_cnt <= 2'b11;
      active <= 1'b0;
   end
   else begin
      shift_cnt <= shift_cnt + 1;
      if (shift_cnt == 2'b11) begin
         trig_sr <= datain;
         if (datain == 4'h0) begin
            active <= 1'b0;
         end
         else begin
            active <= 1'b1;
         end
      end
      else begin
         trig_sr <= {trig_sr[2:0], 1'b0};
      end
   end
end

assign trig_done = active & (shift_cnt == 2'b00);
    // yes, I realize this is not the 'end' of the word. It works
    // best with the FIFO logic
assign trig_out = trig_sr[3];

endmodule
