library ieee;
use ieee.std_logic_1164.all;

package register_pkg is
	subtype reg is std_logic_vector(31 downto 0);
	type reg_matrix is array (natural range <>) of reg;

	-- CONTROL registers
	constant N_CTRL_REGS							: integer	:= 3;
	
	constant reset_from_software_REG				: natural := 0;
	constant reset_from_software_RANGE				: natural := 0;
	
	constant enable_REG								: natural := 0;
	constant enable_RANGE							: natural := 1;
	
	constant trig_int_REG							: natural := 1;
	type trig_int_RANGE								is range 7 downto 0;
	
	constant n_hits_REG								: natural := 2;
	type n_hits_RANGE								is range 7 downto 0;

	-- STATUS registers
	constant N_STATUS_REGS							: integer	:= 24;
	
	constant DATE_HEX_REG							: natural := 0;
	type DATE_HEX_RANGE								is range 31 downto 0;
			
	constant TIME_HEX_REG							: natural := 1;
	type TIME_HEX_RANGE								is range 31 downto 0;
	
	constant GIT_HASH_0_REG							: natural := 2;
	type GIT_HASH_0_RANGE							is range 31 downto 0;
		
	constant GIT_HASH_1_REG							: natural := 3;
	type GIT_HASH_1_RANGE							is range 31 downto 0;
		
	constant GIT_HASH_2_REG							: natural := 4;
	type GIT_HASH_2_RANGE							is range 31 downto 0;
		
	constant GIT_HASH_3_REG							: natural := 5;
	type GIT_HASH_3_RANGE							is range 31 downto 0;	
	
	constant GIT_HASH_4_REG							: natural := 6;
	type GIT_HASH_4_RANGE							is range 31 downto 0; 
	
	constant cnt_tot_0_msb_REG						: natural := 7;
	type cnt_tot_0_msb_RANGE						is range 31 downto 0;
	constant cnt_tot_0_lsb_REG						: natural := 8;
	type cnt_tot_0_lsb_RANGE						is range 31 downto 0;
	
	constant cnt_tot_1_msb_REG						: natural := 9;
	type cnt_tot_1_msb_RANGE						is range 31 downto 0;
	constant cnt_tot_1_lsb_REG						: natural := 10;
	type cnt_tot_1_lsb_RANGE						is range 31 downto 0;

	constant cnt_tot_2_msb_REG						: natural := 11;
	type cnt_tot_2_msb_RANGE						is range 31 downto 0;
	constant cnt_tot_2_lsb_REG						: natural := 12;
	type cnt_tot_2_lsb_RANGE						is range 31 downto 0;

	constant cnt_tot_3_msb_REG						: natural := 13;
	type cnt_tot_3_msb_RANGE						is range 31 downto 0;
	constant cnt_tot_3_lsb_REG						: natural := 14;
	type cnt_tot_3_lsb_RANGE						is range 31 downto 0;
	
	constant cnt_hit_0_msb_REG						: natural := 15;
	type cnt_hit_0_msb_RANGE						is range 31 downto 0;
	constant cnt_hit_0_lsb_REG						: natural := 16;
	type cnt_hit_0_lsb_RANGE						is range 31 downto 0;
	
	constant cnt_hit_1_msb_REG						: natural := 17;
	type cnt_hit_1_msb_RANGE						is range 31 downto 0;
	constant cnt_hit_1_lsb_REG						: natural := 18;
	type cnt_hit_1_lsb_RANGE						is range 31 downto 0;

	constant cnt_hit_2_msb_REG						: natural := 19;
	type cnt_hit_2_msb_RANGE						is range 31 downto 0;
	constant cnt_hit_2_lsb_REG						: natural := 20;
	type cnt_hit_2_lsb_RANGE						is range 31 downto 0;

	constant cnt_hit_3_msb_REG						: natural := 21;
	type cnt_hit_3_msb_RANGE						is range 31 downto 0;
	constant cnt_hit_3_lsb_REG						: natural := 22;
	type cnt_hit_3_lsb_RANGE						is range 31 downto 0;
	
	constant fifo_trig_cnt_REG						: natural := 23;
	type fifo_trig_cnt_RANGE						is range 10 downto 0;

	constant REGS_AXI_ADDR_WIDTH : integer := 31;

end package;
