library IEEE, example;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity adder is
  generic (
    constant DATA_WIDTH : positive := 16;
    constant FIFO_DEPTH : positive := 256
    );
  port (
    CLK     : in  std_logic;
    DataIn  : in  std_logic_vector (DATA_WIDTH - 1 downto 0);
    DataOut : out std_logic_vector (DATA_WIDTH - 1 downto 0)
    );
end adder;

architecture Behavioral of adder is
begin
  add_proc : process(clk)
  begin
    if rising_edge(clk) then
      DataOut <= std_logic_vector(unsigned(DataIn) + 1);
    end if;
  end process;  -- add_proc

end Behavioral;
-- Appending text for diff release 
